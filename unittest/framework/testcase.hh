﻿#pragma once

#include <string>
#include <list>

namespace k {
namespace test {

using ErrorList = std::list<std::string>;

/// 测试用例
class Testcase {
    std::string name_; // 用例名称
    bool run_; // 是否需要运行
    ErrorList errorList_; // 本次测试错误信息

public:
    // 构造
    // @param name 用例名称
    Testcase(const std::string& name);
    // 析构
    virtual ~Testcase();
    // 运行用例
    virtual void run() = 0;
    // 取得用例名称
    const std::string& getName() const;
    // 禁用用例
    void disable();
    // 开启用例，默认开启
    void enable();
    // 是否开启
    bool isEnable();
    // 运行是否成功
    bool isSuccess();
    // 设置错误信息
    // @param error 错误信息
    void setError(const std::string& error);
    // 取得错误信息列表
    const ErrorList& getErrorList() const;
    // 清理错误
    void clearError();
};

}
}
