/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "util.h"

namespace klogger {

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)
int gettimeofday(struct timeval *tp, void *tzp) {
  (void)tzp;
  time_t clock;
  struct tm tm;
  SYSTEMTIME wtm;
  GetLocalTime(&wtm);
  tm.tm_year = wtm.wYear - 1900;
  tm.tm_mon = wtm.wMonth - 1;
  tm.tm_mday = wtm.wDay;
  tm.tm_hour = wtm.wHour;
  tm.tm_min = wtm.wMinute;
  tm.tm_sec = wtm.wSecond;
  tm.tm_isdst = -1;
  clock = mktime(&tm);
  tp->tv_sec = (long)clock;
  tp->tv_usec = wtm.wMilliseconds * 1000;
  return (0);
}
#endif // _WINDOWS

void getDateTime(timeval *tp, tm *t) {
  time_t timestamp = time(0);
  gettimeofday(tp, 0);
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)
  localtime_s(t, &timestamp);
#else
  localtime_r(&timestamp, t);
#endif // _WIN32
}

void sleep(int ms) {
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)
  Sleep(ms);
#else
  usleep(ms * 1000);
#endif // _WIN32
}

const std::string &getLevelString(int level) {
  static const std::string VERBOSE_STRING = "[verb]";
  static const std::string INFORMATION_STRING = "[info]";
  static const std::string DEBUG_STRING = "[debu]";
  static const std::string WARNING_STRING = "[warn]";
  static const std::string EXCEPTION_STRING = "[exce]";
  static const std::string FAILURE_STRING = "[fail]";
  static const std::string FATAL_STRING = "[fata]";
  static const std::string STAT_STRING = "[stat]";
  static const std::string UNKNOWN_STRING = "[unkn]";

  switch (level) {
  case Logger::VERBOSE:
    return VERBOSE_STRING;
  case Logger::INFORMATION:
    return INFORMATION_STRING;
  case Logger::DIAGNOSE:
    return DEBUG_STRING;
  case Logger::WARNING:
    return WARNING_STRING;
  case Logger::EXCEPTION:
    return EXCEPTION_STRING;
  case Logger::FAILURE:
    return FAILURE_STRING;
  case Logger::FATAL:
    return FATAL_STRING;
  case Logger::STAT:
    return STAT_STRING;
  default:
    break;
  }

  return UNKNOWN_STRING;
}

} // namespace klogger
