/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONSOLE_APPENDER_H
#define CONSOLE_APPENDER_H

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <list>
#include <memory>
#include <string>
#include <unordered_map>

#include "klogger/interface/logger.h"
#include "mode.h"
#include "scope_lock.h"
#include "util.h"

class LoggerImpl;

using namespace klogger;

class ConsoleAppender : public Appender {
public:
  ConsoleAppender(LoggerImpl *impl_ptr, const std::string &name,
                  const std::string &attribute);
  virtual ~ConsoleAppender();
  virtual void write(int level, const char *format, ...);
  virtual void setLevel(int level);
  virtual int getLevel();
  virtual void destroy();
  virtual void reload(const std::string &attribute);
  virtual void show_level(bool flag);
  virtual void show_name(bool flag);

private:
  /**
   * 解析配置字符串
   * @param attribute 配置字符串
   */
  void parseAttribute(const std::string &attribute);

  /**
   * 取得配置值
   * @param name 配置键名称
   * @param attribute 配置
   * @return 配置值
   */
  std::string getAttribute(const std::string &name,
                           const std::string &attribute);

  /**
   * 写日志行
   * @param level 日志等级字符串
   * @param format 日志格式
   * @param va_ptr 可变参数宏
   */
  void write(const std::string &level, const char *format, va_list va_ptr);

  /**
   * 写日志行, JSON格式
   * @param level 日志等级字符串
   * @param format 日志格式
   * @param va_ptr 可变参数宏
   */
  void write_json(const std::string &level, const char *format, va_list va_ptr);

private:
  std::string _line{"[%y%m%d-%H:%M:%S:%U]"}; ///< 日志行前缀
  std::string _name{"[anony]"};   ///< 添加器在日志行中显示的名称
  std::string _realName{"anony"}; ///< 添加器名称
  bool _flush{false};             ///< flush标志
  LoggerImpl *_impl{nullptr};     ///< Logger实现类
  Pattern _pattern;               ///< 日志行前缀匹配器
  RecursiveLock _lock;            ///< 锁
  int _level{Logger::MIN};        ///< 日志等级
  bool _same_log_count{false};    ///< 相同日志追加数量
  using LogMap = std::unordered_map<std::string, std::size_t>;
  LogMap _log_map; ///< 历史日志
  bool show_log_level_{true};
  bool show_log_name_{true};
  enum class Format {
    TXT = 1, ///< 文本
    JSON,    ///< JSON格式
  };
  Format _format{Format::TXT};                    ///< 日志文本格式
  static const std::size_t BUF_LEN = 1024 * 1024; ///< 单条日志最大长度
};

#endif // CONSOLE_APPENDER_H
