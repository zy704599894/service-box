﻿#include "load_balancer/load_balance.h"

namespace kratos {
namespace loadbalance {

std::int32_t gcd(std::int32_t a, std::int32_t b) {
  while (b != 0) {
    std::int32_t r = b;
    b = a % b;
    a = r;
  }
  return a;
};

class WRRSBalancer : public ILoadBalancer {
public:
  WRRSBalancer() = default;
  virtual ~WRRSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override;

  static auto get_name() -> const std::string {
    return "WEIGHTED_ROUND_ROBIN_SCHEDULING";
  }
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Weighted_Round_Robin_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<WRRSBalancer>();
  }

private:
  static bool weight_round_robin_register_;
  std::int32_t last_select_index_{-1};
  std::int32_t tot_weight_{0};
  std::int32_t max_weight_{0};
  std::int32_t current_weight_{0};
  std::int32_t gcd_weight_{0};
  std::int32_t server_count_{0};
  std::vector<LoadBalanceNodeWeakPtr> nodes_;
};

auto WRRSBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  nodes_.emplace_back(lbnode);
  std::int32_t weight = lbnode.lock()->get_weight();
  tot_weight_ += weight;
  max_weight_ = std::max(max_weight_, weight);
  gcd_weight_ = gcd_weight_ ? gcd(weight, gcd_weight_) : weight;
  server_count_++;
  return true;
}

auto WRRSBalancer::get_next(const std::string &/*ip*/) -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  if (server_count_ == 1) {
    return nodes_[0];
  }
  while (true) {
    last_select_index_ = (last_select_index_ + 1) % server_count_;
    if (last_select_index_ == 0) {
      current_weight_ = current_weight_ - gcd_weight_;
      if (current_weight_ < 0) {
        current_weight_ = max_weight_;
        if (current_weight_ == 0) {
          return LoadBalanceNodeWeakPtr();
        }
      }
    }
    if (nodes_[last_select_index_].expired()) {
      return LoadBalanceNodeWeakPtr();
    }
    if (nodes_[last_select_index_].lock()->get_weight() >= current_weight_) {
      return nodes_[last_select_index_];
    }
  }
  return LoadBalanceNodeWeakPtr();
}

auto WRRSBalancer::clear() -> void {
  last_select_index_ = -1;
  tot_weight_ = 0;
  max_weight_ = 0;
  current_weight_ = 0;
  gcd_weight_ = 0;
  server_count_ = 0;
  nodes_.clear();
}

auto WRRSBalancer::reset_balancer() -> void {
  last_select_index_ = -1;
  tot_weight_ = 0;
  max_weight_ = 0;
  current_weight_ = 0;
  gcd_weight_ = 0;
  for (auto &it : nodes_) {
    if (it.expired()) {
      continue;
    }
    std::int32_t weight = it.lock()->get_weight();
    tot_weight_ += weight;
    max_weight_ = std::max(max_weight_, weight);
    gcd_weight_ = gcd_weight_ ? gcd(weight, gcd_weight_) : weight;
  }
}

Registe_Balancer(WRRSBalancer, weight_round_robin_,
                 BalancerMod::Weighted_Round_Robin_Scheduling,
                 WRRSBalancer::creator)

} // namespace loadbalance
} // namespace kratos
