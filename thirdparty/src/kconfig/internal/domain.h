/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DOMAIN_H
#define DOMAIN_H

#include <map>
#include <string>

#include "attribute_impl.hpp"
#include "config.h"

using namespace kconfig;

class Domain;

using DomainPtr = std::shared_ptr<Domain>;

class Domain : public AttributeImpl<ZoneAttribute, Attribute::ZONE> {
public:
  Domain(const std::string &name, Domain *parent);
  virtual ~Domain();
  virtual bool has(const std::string &name) override;
  virtual Attribute *get(const std::string &name) override;
  virtual bool hasNext() override;
  virtual Attribute *next() override;
  virtual AttributePtr doOperate(int op, AttributePtr rhs) override;
  virtual auto to_string() -> std::string override;

  AttributePtr get_ptr(const std::string &name);
  Domain *getParent();
  DomainPtr newChild(const std::string &name);
  bool addAttribute(const std::string &name, AttributePtr attribute);
  void removeAttribute(const std::string& name);
  void trace(const std::string &tab);
  void merge_from(Domain *rhs);

private:
  Attribute *findDomain(const std::string &name);
  AttributePtr findDomainPtr(const std::string &name);
  Attribute *findAttribute(const std::string &name);
  AttributePtr findAttributePtr(const std::string &name);
  Attribute *find(const std::string &name);
  AttributePtr find_ptr(const std::string &name);

private:
  std::map<std::string, AttributePtr> _attributes;
  std::map<std::string, DomainPtr> _domains;
  std::map<std::string, AttributePtr> _all;
  std::map<std::string, AttributePtr>::iterator _cursor;

  bool _fired;
  Domain *_parent;
  std::string _name;
};

#endif // DOMAIN_H
