#pragma once

#include <string>

#include "config.h"
#include "attribute_impl.hpp"

using namespace kconfig;

class BoolImpl : public AttributeImpl<BoolAttribute, Attribute::BOOL> {
public:
  BoolImpl(bool value);
  virtual ~BoolImpl();
  virtual bool get() override;
  virtual AttributePtr doOperate(int op, AttributePtr rhs) override;
  virtual auto to_string()->std::string override;

private:
  bool _value;
};
