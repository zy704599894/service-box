# -*- coding: UTF-8 -*-

import zipfile
import os
import platform
import sys
import subprocess
import ctypes
import shutil
import stat
import getpass

class WindowsEnv:
    def __init__(self, def_tag):
        self.def_tag = def_tag

    def check_remove(self):
        if os.path.exists('service-box-windows-dependency'):
            os.system("rd/s/q  service-box-windows-dependency")
        return True
    
    def pull_dependency(self):
        #目前来看这个应该是没有版本问题的
        if not os.path.exists('service-box-windows-dependency'):
            if os.system("git clone https://gitee.com/dennis-kk/service-box-windows-dependency") != 0:
                print("Pull windows dependency failed")
                return False
        else:
            if os.system("git pull") != 0:
                print("Pull windows dependency failed")
                return False
        return True

    def build(self):
        if platform.system() != "Windows":
            print("Only run on windows")
            exit(-1)
        if os.system("java.exe -version") != 0:
            print("Please install JDK")
            exit(-1)
        
        if not self.pull_dependency():
            exit(-1)
        
        zip = zipfile.ZipFile('./service-box-windows-dependency/service-box-windows-dependency.zip', 'r')
        for file in zip.namelist(): 
            zip.extract(file, "./service-box-windows-dependency/")
        zip.close()
        if not os.path.exists("../thirdparty/include/google/protobuf"):
            shutil.copytree("./service-box-windows-dependency/protobuf/include/google", "../thirdparty/include/google")
        shutil.copyfile('./service-box-windows-dependency/protobuf/lib/libprotobufd.lib', '../thirdparty/lib/win/debug/libprotobufd.lib')
        shutil.copyfile('./service-box-windows-dependency/protobuf/lib/libprotobuf.lib', '../thirdparty/lib/win/release/libprotobuf.lib')
        return True

if __name__ == "__main__":
    def_tag = 'develop'
    if len(sys.argv) <= 1:
        print("build service box local env with default branch: ", def_tag)
    else:
        def_tag=sys.argv[1] 
        print("[service-box] build local env with input branch: ", def_tag)

    #仓库检查已经提前到上级脚本做掉了

    env = WindowsEnv(def_tag)
    if env.build() == False:
        exit(1)
    else:
        exit(0)
        