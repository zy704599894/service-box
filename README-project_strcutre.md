# 工程结构

```
service-box
├── build													// service box 安装脚本
├── publish													// service box 发布脚本
├── src														// service box 代码
│	├── repo												// repo 服务仓库 & 服务工具
│	└── ...
├── thirdparty												// 第三方库
├── tools													// service box启动/关闭本机环境脚本，默认容器启动脚本
├── unittest												// 单元测试框架 以及 测试用例
└── win-proj												// windows 工程文件目录
```



## 代码结构

```
.
├── ...
├── src
│   ├── argument											// service box 启动参数接口
│   ├── box													// service box 核心启动类、核心类实现、接口
│   ├── command												// 外部命令处理器接口
│   ├── config												// service box 配置接口
│   ├── console												// service box web console接口
│   ├── csv													// CSV配置管理器接口
│   ├── detail												// service box接口实现类、内部类
│   │   └── zookeeper										// 服务注册/发现zookeeper实现
│   ├── http												// HTTP客户端、服务器接口
│   ├── lang												// 日志多国语言管理接口
│   ├── lib													// service box主函数入口
│   ├── redis												// Redis客户端接口
│   ├── scheduler											// 定时器管理器接口
│   ├── service_finder										// 服务发现接口
│   ├── service_register									// 服务注册接口
│   ├── time												// 时间系统接口
│   ├── util												// 工具库
│   │    ├── lua											// lua工具库、lua脚本引擎、lua调试器
│   │    └── websocket										// WebSocket服务器
│	└── ...
└── ...
```



## 单元测试

```
.
├── ...
├── unittest
│   ├── framework											// 单元测试框架
│   ├── src													// 所有测试用例
│   └── ...
└── ...
```

