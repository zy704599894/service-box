﻿#pragma once

#include "root/coroutine/coroutine.h"
#include <functional>

namespace kratos {
namespace service {
/**
 * 协程管理器.
 */
class CoManager {
public:
  /**
   * 析构.
   *
   */
  virtual ~CoManager() {}
  /**
   * 产生一个协程.
   *
   * \param co_func 协程函数
   * \return 协程ID
   */
  virtual auto spawn(std::function<void(void)> co_func)
      -> coroutine::CoroID = 0;
  /**
   * 关闭协程.
   *
   * \param co_id 协程ID
   * \return
   */
  virtual auto close(coroutine::CoroID co_id) -> void = 0;
};
} // namespace service
} // namespace kratos
