﻿#pragma once

#include "root/rpc_interface_descriptor.hh"
#include "include/json/json.h"

namespace kratos {
namespace service {

/**
 * @brief rpc::InterfaceDescriptor实现类
 */
class InterfaceDescriptorImpl : public rpc::InterfaceDescriptor {
  Json::Value descriptor_; ///< 接口描述

 public:
  /**
   * @brief 构造
   */
  InterfaceDescriptorImpl();
  virtual ~InterfaceDescriptorImpl();
  virtual auto parse(const std::string &description) -> bool override;
  virtual auto equal(const rpc::InterfaceDescriptor &other) const
      -> bool override;
};

/**
 * @brief rpc::InterfaceDescriptorFactory实现类
 */
class InterfaceDescriptorFactoryImpl : public rpc::InterfaceDescriptorFactory {
public:
  /**
   * @brief 构造
   */
  InterfaceDescriptorFactoryImpl();
  virtual ~InterfaceDescriptorFactoryImpl();
  virtual auto create_descriptor() -> rpc::InterfaceDescriptorPtr override;
};

} // namespace service
} // namespace kratos
