﻿#pragma once

#include <string>

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)

namespace kratos {
namespace service {
class ServiceBox;
} // namespace service
} // namespace kratos

namespace kratos {
namespace util {

/**
 * 获取当前调用堆栈.
 */
class StackTrace {
  StackTrace() = delete;
  StackTrace(const StackTrace &) = delete;
  StackTrace(StackTrace &&) = delete;
  const StackTrace &operator=(const StackTrace &) = delete;
  const StackTrace &operator=(StackTrace &&) = delete;

public:
  constexpr static int MAX_FRAMES = 64; ///< 最大堆栈深度

public:
  /**
   * 获取当前调用堆栈，打印深度最大为max_frames.
   *
   * \param max_frames 打印深度最大值，默认为MAX_FRAMES
   * \return 堆栈信息
   */
  auto static get_stack_frame_info(int max_frames = MAX_FRAMES) -> std::string;
  /**
   * 获取未处理的异常现场堆栈.
   * 
   * \param except_info 异常信息
   * \param max_frames 打印深度最大值，默认为MAX_FRAMES
   * \return 堆栈信息
   */
  auto static get_unhandled_exception_stack_frame_info(void* except_info,
      int max_frames = MAX_FRAMES) -> std::string;
  /**
   * 安装SEH异常顶层处理器.
   *
   * \return true或false
   */
  auto static install_seh_handler(kratos::service::ServiceBox *box) -> bool;
  /**
   * 卸载SEH异常顶层处理器.
   * 
   * \return 
   */
  auto static uninstall_seh_handler() -> void;
  /**
   * 安装系统异常处理.
   * 
   * \return true或false
   */
  auto static install_system_exception(kratos::service::ServiceBox *box)
      -> bool;
  /**
   * 西在系统异常处理.
   * 
   * \return 
   */
  auto static uninstall_system_exception() -> void;
  /**
   * SEH异常处理.
   * 
   * \param exception_pointers
   * \return 
   */
  auto static seh_handler(void* exception_pointers) -> long;
};

} // namespace util
} // namespace kratos

#endif // _WIN32
