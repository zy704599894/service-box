#include "json_pb.hh"

#include "box/box_network.hh"
#include "config/box_config.hh"
#include "json/json.h"

#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/message.h>
#include <google/protobuf/reflection.h>

#include <filesystem>
#include <sstream>
#include <stdexcept>
#include <string>

#ifdef GetMessage
#undef GetMessage
#endif // GetMessage

namespace kratos {
namespace loop {

#define ExceWrapper(field, expr) \
  try {\
    expr;\
  } catch (std::exception& ex) {\
    auto reason = field->name() + ",";\
    throw std::runtime_error(reason + ex.what());\
  }

JsonPb::JsonPb(kratos::service::BoxNetwork *network) { network_ = network; }

JsonPb::~JsonPb() {}

auto JsonPb::load() -> bool {
  // 加载配置
  if (!network_->get_config().has_attribute("rpc_root_dir")) {
    throw std::runtime_error("Need rpc_root_dir attribute but not found");
    return false;
  }
  auto rpc_root_dir = network_->get_config().get_string("rpc_root_dir");
  msg_factory_ptr_ = std::make_unique<kratos::util::MsgFactory>(network_);
  if (!msg_factory_ptr_->load(rpc_root_dir, rpc_root_dir)) {
    throw std::runtime_error("Apply RPC configuration failed");
    return false;
  }
  return true;
}

auto JsonPb::to_rpc(Json::Value &value, char *&rpc_proto, std::size_t &length,
                    const util::MethodType *method_type_ptr) noexcept(false)
    -> bool {
  if (!value.isMember("args")) {
    throw std::runtime_error("Invalid format, need 'args'");
    return false;
  }
  auto &arg_node = value["args"];
  auto arg_count = std::size_t(arg_node.size());
  if (arg_count != method_type_ptr->arg_name_vector.size()) {
    throw std::runtime_error(
        "Invalid argument number, need " +
        std::to_string(method_type_ptr->arg_name_vector.size()) + " but " +
        std::to_string(arg_count) + "given");
    return false;
  }
  for (const auto &arg_name : method_type_ptr->arg_name_vector) {
    if (arg_node.isMember(arg_name.pb_name)) {
      // JSON内字段存在这个PB参数名字
      continue;
    }
    if (!arg_node.isMember(arg_name.decl_name)) {
      // 没有PB名字也没有形参名
      throw std::runtime_error("Method argument name invalid");
      return false;
    }
    // 处理不存在的情况
    arg_node[arg_name.pb_name] = arg_node[arg_name.decl_name];
    arg_node.removeMember(arg_name.decl_name);
  }
  auto *msg_ptr = msg_factory_ptr_->new_call_param(
      method_type_ptr->service_name, method_type_ptr->method_name);
  if (!msg_ptr) {
    throw std::runtime_error("Method not found, " +
                             method_type_ptr->service_name + ":" +
                             method_type_ptr->method_name);
    return false;
  }
  if (!encode(arg_node, msg_ptr)) {
    return false;
  }
  buffer_.clear();
  auto res = buffer_.apply(msg_ptr->ByteSizeLong());
  if (!msg_ptr->SerializeToArray(res.first, (int)res.second)) {
    throw std::runtime_error("Invalid format, SerializeToArray");
    return false;
  }
  rpc_proto = res.first;
  length = res.second;
  return true;
}

auto JsonPb::to_json(rpc::ServiceUUID uuid, rpc::MethodID method_id,
                     Json::Value &value, const char *rpc_proto,
                     std::size_t length) noexcept(false) -> bool {
  auto *msg_ptr = msg_factory_ptr_->new_call_return(uuid, method_id);
  if (!msg_ptr) {
    // 不会发生
    throw std::runtime_error("Method not found");
    return false;
  }
  if (!msg_ptr->ParseFromArray(rpc_proto, (int)length)) {
    throw std::runtime_error("Invalid format, ParseFromArray");
    return false;
  }
  auto json_ptr = decode(*msg_ptr);
  if (!json_ptr) {
    return false;
  }
  value = std::move(*json_ptr);
  if (value.isMember("_1")) {
    value["ret"] = value["_1"];
    value.removeMember("_1");
  }
  return true;
}

auto JsonPb::get_method_info(const std::string &service_name,
                             const std::string &method_name)
    -> std::pair<rpc::ServiceUUID, rpc::MethodID> {
  return msg_factory_ptr_->get_method_info(service_name, method_name);
}

auto JsonPb::is_oneway(rpc::ServiceUUID uuid, rpc::MethodID method_id) -> bool {
  auto *method_type = msg_factory_ptr_->get_type(uuid, method_id);
  if (!method_type) {
    return false;
  }
  return method_type->oneway;
}

auto JsonPb::get_method_type(rpc::ServiceUUID uuid, rpc::MethodID method_id)
    -> const util::MethodType * {
  auto *method_type = msg_factory_ptr_->get_type(uuid, method_id);
  if (!method_type) {
    return nullptr;
  }
  return method_type;
}

auto JsonPb::decode(const ProtobufMessage &pb_msg)
    -> std::unique_ptr<Json::Value> {
  const auto *descriptor = pb_msg.GetDescriptor();
  const auto *reflection = pb_msg.GetReflection();
  auto root_ptr = std::make_unique<Json::Value>(Json::ValueType::objectValue);
  // 遍历pb_msg字段
  for (int i = 0; i < descriptor->field_count(); ++i) {
    const auto *field = descriptor->field(i);
    if (!field->is_repeated() && !reflection->HasField(pb_msg, field)) {
      //
      // 设置默认字段值
      //
      Json::Value default_value;
      auto type = field->cpp_type();
      switch (type) {
      case ProtobufFieldDescriptor::CPPTYPE_INT32:
      case ProtobufFieldDescriptor::CPPTYPE_UINT32:
        default_value = 0;
        break;
      case ProtobufFieldDescriptor::CPPTYPE_INT64:
      case ProtobufFieldDescriptor::CPPTYPE_UINT64:
        default_value = 0;
        break;
      case ProtobufFieldDescriptor::CPPTYPE_BOOL:
        default_value = false;
        break;
      case ProtobufFieldDescriptor::CPPTYPE_FLOAT:
      case ProtobufFieldDescriptor::CPPTYPE_DOUBLE:
        default_value = .0f;
        break;
      case ProtobufFieldDescriptor::CPPTYPE_STRING:
        default_value = "";
        break;
      case ProtobufFieldDescriptor::CPPTYPE_ENUM:
        default_value = 0;
        break;
      default:
        break;
      }
      if (!default_value.isNull()) {
        (*root_ptr)[field->name()] = default_value;
      }
    } else {
      auto obj_ptr = on_field(pb_msg, field);
      if (obj_ptr) {
        (*root_ptr)[field->name()] = *obj_ptr;
      }
    }
  }
  return root_ptr;
}

auto JsonPb::on_field(const ProtobufMessage &msg,
                      const ProtobufFieldDescriptor *field)
    -> std::unique_ptr<Json::Value> {
  if (field->is_repeated()) {
    //
    // repeated/map字段
    //
    if (field->is_map()) {
      //
      // map字段
      //
      return on_map_field(msg, field);
    } else {
      //
      // repeated字段
      //
      return on_repeated_field(msg, field);
    }
  } else {
    switch (field->type()) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetBool(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT32: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetInt32(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT64: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetInt64(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetUInt32(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetUInt64(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetFloat(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetDouble(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_STRING: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetString(msg, field).c_str());
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      return decode(msg.GetReflection()->GetMessage(msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
      return std::make_unique<Json::Value>(
          msg.GetReflection()->GetEnumValue(msg, field));
      break;
    }
    default:
      break;
    }
  }
  return nullptr;
}

auto JsonPb::on_map_field(const ProtobufMessage &msg,
                          const ProtobufFieldDescriptor *field)
    -> std::unique_ptr<Json::Value> {
  auto dict_obj_ptr =
      std::make_unique<Json::Value>(Json::ValueType::objectValue);
  auto count = msg.GetReflection()->FieldSize(msg, field);
  for (int i = 0; i < count; i++) {
    const auto &message_map =
        msg.GetReflection()->GetRepeatedMessage(msg, field, i);
    const auto *descriptor = message_map.GetDescriptor();
    const auto *key_field_descriptor = descriptor->field(0);
    const auto *value_field_descriptor = descriptor->field(1);
    auto key_obj_ptr = on_map_field_key(message_map, key_field_descriptor);
    auto value_obj_ptr =
        on_map_field_value(message_map, value_field_descriptor);
    // set map key, value
    (*dict_obj_ptr)[key_obj_ptr->asString()] = *value_obj_ptr;
  }
  return dict_obj_ptr;
}

auto JsonPb::on_map_field_key(const ProtobufMessage &msg,
                              const ProtobufFieldDescriptor *field)
    -> std::unique_ptr<Json::Value> {
  auto key_type = field->type();
  switch (key_type) {
  case ProtobufFieldDescriptor::Type::TYPE_INT32: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetInt32(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_INT64: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetInt64(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_STRING: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetString(msg, field).c_str());
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetUInt32(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetUInt64(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetEnumValue(msg, field));
    break;
  }
  default:
    break;
  }
  return nullptr;
}

auto JsonPb::on_map_field_value(const ProtobufMessage &msg,
                                const ProtobufFieldDescriptor *field)
    -> std::unique_ptr<Json::Value> {
  auto value_type = field->type();
  switch (value_type) {
  case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetBool(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_INT32: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetInt32(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_INT64: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetInt64(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetUInt32(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetUInt64(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetFloat(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetDouble(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_STRING: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetString(msg, field).c_str());
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
    return decode(msg.GetReflection()->GetMessage(msg, field));
    break;
  }
  case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
    return std::make_unique<Json::Value>(
        msg.GetReflection()->GetEnumValue(msg, field));
    break;
  }
  default:
    break;
  }
  return nullptr;
}

auto JsonPb::on_repeated_field(const ProtobufMessage &msg,
                               const ProtobufFieldDescriptor *field)
    -> std::unique_ptr<Json::Value> {
  auto count = msg.GetReflection()->FieldSize(msg, field);
  auto list_obj_ptr =
      std::make_unique<Json::Value>(Json::ValueType::arrayValue);
  if (field->type() == ProtobufFieldDescriptor::Type::TYPE_MESSAGE) {
    for (auto i = 0; i < count; i++) {
      const auto &repeated_message =
          msg.GetReflection()->GetRepeatedMessage(msg, field, i);
      auto msg_obj_ptr = decode(repeated_message);
      list_obj_ptr->append(*msg_obj_ptr);
    }
  } else {
    for (int i = 0; i < count; i++) {
      switch (field->type()) {
      case ProtobufFieldDescriptor::Type::TYPE_BOOL: {
        list_obj_ptr->append(
            msg.GetReflection()->GetRepeatedBool(msg, field, i));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_INT32: {
        list_obj_ptr->append(
            msg.GetReflection()->GetRepeatedInt32(msg, field, i));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_INT64: {
        list_obj_ptr->append(
            msg.GetReflection()->GetRepeatedInt64(msg, field, i));
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
        auto value = msg.GetReflection()->GetRepeatedUInt32(msg, field, i);
        list_obj_ptr->append(value);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
        auto value = msg.GetReflection()->GetRepeatedUInt64(msg, field, i);
        list_obj_ptr->append(value);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_FLOAT: {
        auto value = msg.GetReflection()->GetRepeatedFloat(msg, field, i);
        list_obj_ptr->append(value);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_DOUBLE: {
        auto value = msg.GetReflection()->GetRepeatedDouble(msg, field, i);
        list_obj_ptr->append(value);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
        auto value = msg.GetReflection()->GetRepeatedEnum(msg, field, i);
        auto num = value->number();
        list_obj_ptr->append(num);
        break;
      }
      case ProtobufFieldDescriptor::Type::TYPE_STRING: {
        auto value = msg.GetReflection()->GetRepeatedString(msg, field, i);
        list_obj_ptr->append(value);
        break;
      }
      default:
        break;
      }
    }
  }
  return list_obj_ptr;
}

auto JsonPb::on_object_value(const Json::Value &obj_ptr, ProtobufMessage &msg,
                             const ProtobufReflection *ref,
                             const ProtobufFieldDescriptor *field) -> void {
  if (field->is_repeated()) {
    if (field->is_map()) {
      on_object_map(obj_ptr, msg, ref, field);
    } else {
      on_object_repeated_value(obj_ptr, msg, ref, field);
    }
  } else {
    switch (field->type()) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL:
      ExceWrapper(field, ref->SetBool(&msg, field, obj_ptr.asBool()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT32:
      ExceWrapper(field, ref->SetInt32(&msg, field, obj_ptr.asInt()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT64:
      ExceWrapper(field, ref->SetInt64(&msg, field, obj_ptr.asInt64()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT32:
      ExceWrapper(field, ref->SetUInt32(&msg, field, obj_ptr.asUInt()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT64:
      ExceWrapper(field, ref->SetUInt64(&msg, field, obj_ptr.asUInt64()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
      ExceWrapper(field, ref->SetFloat(&msg, field, obj_ptr.asFloat()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
      ExceWrapper(field, ref->SetDouble(&msg, field, obj_ptr.asDouble()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_STRING: {
      ExceWrapper(field, ref->SetString(&msg, field, obj_ptr.asString()));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      encode(obj_ptr, ref->MutableMessage(&msg, field));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
      ExceWrapper(field, ref->SetEnumValue(&msg, field, obj_ptr.asInt()));
      break;
    }
    default:
      break;
    }
  }
}

auto JsonPb::on_object_map(const Json::Value &obj_ptr, ProtobufMessage &msg,
                           const ProtobufReflection *ref,
                           const ProtobufFieldDescriptor *field) -> void {
  for (const auto &key : obj_ptr.getMemberNames()) {
    auto *pair_message = ref->AddMessage(&msg, field);
    auto *pair_reflection = pair_message->GetReflection();
    const auto *key_descriptor = field->message_type()->field(0);
    const auto *value_descriptor = field->message_type()->field(1);
    auto value_obj = obj_ptr[key];
    auto key_type = key_descriptor->type();
    switch (key_type) {
    case ProtobufFieldDescriptor::Type::TYPE_INT32: {
      pair_reflection->SetInt32(pair_message, key_descriptor, std::stoi(key));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_INT64: {
      pair_reflection->SetInt64(pair_message, key_descriptor, std::stoll(key));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_STRING: {
      pair_reflection->SetString(pair_message, key_descriptor, key);
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT32: {
      pair_reflection->SetUInt32(pair_message, key_descriptor, std::stoul(key));
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_UINT64: {
      pair_reflection->SetUInt64(pair_message, key_descriptor,
                                 std::stoull(key));
      break;
    }
    default:
      break;
    }
    auto value_type = value_descriptor->type();
    switch (value_type) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL:
      ExceWrapper(value_descriptor, pair_reflection->SetBool(pair_message, value_descriptor,
                               value_obj.asBool()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT32:
      ExceWrapper(value_descriptor, pair_reflection->SetInt32(pair_message, value_descriptor,
                                value_obj.asInt()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT64:
      ExceWrapper(value_descriptor, pair_reflection->SetInt64(pair_message, value_descriptor,
                                value_obj.asInt64()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT32:
      ExceWrapper(value_descriptor, pair_reflection->SetUInt32(pair_message, value_descriptor,
                                 value_obj.asUInt()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT64:
      ExceWrapper(value_descriptor, pair_reflection->SetUInt64(pair_message, value_descriptor,
                                 value_obj.asUInt64()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
      ExceWrapper(value_descriptor, pair_reflection->SetFloat(pair_message, value_descriptor,
                                value_obj.asFloat()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
      ExceWrapper(value_descriptor, pair_reflection->SetDouble(pair_message, value_descriptor,
                                 value_obj.asDouble()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_STRING:
      ExceWrapper(value_descriptor, pair_reflection->SetString(pair_message, value_descriptor,
                                 value_obj.asString()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      auto *new_message =
          pair_reflection->MutableMessage(pair_message, value_descriptor);
      encode(value_obj, new_message);
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM:
      ExceWrapper(value_descriptor, pair_reflection->SetEnumValue(pair_message, value_descriptor,
                                    value_obj.asInt()));
      break;
    default:
      break;
    }
  }
}

auto JsonPb::on_object_repeated_value(const Json::Value &obj_ptr,
                                      ProtobufMessage &msg,
                                      const ProtobufReflection *ref,
                                      const ProtobufFieldDescriptor *field)
    -> void {
  for (auto &v : obj_ptr) {
    switch (field->type()) {
    case ProtobufFieldDescriptor::Type::TYPE_BOOL:
      ExceWrapper(field, ref->AddBool(&msg, field, v.asBool()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT32:
      ExceWrapper(field, ref->AddInt32(&msg, field, v.asInt()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_INT64:
      ExceWrapper(field, ref->AddInt64(&msg, field, v.asInt64()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT32:
      ExceWrapper(field, ref->AddUInt32(&msg, field, v.asUInt()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_UINT64:
      ExceWrapper(field, ref->AddUInt64(&msg, field, v.asUInt64()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_FLOAT:
      ExceWrapper(field, ref->AddFloat(&msg, field, v.asFloat()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_DOUBLE:
      ExceWrapper(field, ref->AddDouble(&msg, field, v.asDouble()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_STRING:
      ExceWrapper(field, ref->AddString(&msg, field, v.asString()));
      break;
    case ProtobufFieldDescriptor::Type::TYPE_MESSAGE: {
      auto *new_message = ref->AddMessage(&msg, field);
      encode(v, new_message);
      break;
    }
    case ProtobufFieldDescriptor::Type::TYPE_ENUM: {
      ExceWrapper(field, ref->AddEnumValue(&msg, field, v.asInt()));
      break;
    }
    default:
      break;
    }
  }
}

auto JsonPb::encode(const Json::Value &value, ProtobufMessage *msg) -> bool {
  const auto *descriptor = msg->GetDescriptor();
  const auto *reflection = msg->GetReflection();
  for (auto &key : value.getMemberNames()) {
    const auto *field = descriptor->FindFieldByName(key);
    if (field) {
      on_object_value(value[key], *msg, reflection, field);
    }
  }
  return true;
}

} // namespace loop
} // namespace kratos
