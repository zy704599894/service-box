#include "http_loop.hh"

#include "argument/box_argument.hh"
#include "box/box_network.hh"
#include "box/box_network_event.hh"
#include "box/service_box.hh"
#include "detail/box_alloc.hh"
#include "util/box_debug.hh"

#include "json_pb.hh"
#include "knet/knet.h"
#include "root/rpc_defines.h"
#include "util/box_std_allocator.hh"
#include "util/object_pool.hh"
#include "util/os_util.hh"
#include "util/string_util.hh"
#include "json/json.h"

#include "http_parser/http_parser.h"

#include <cstring>
#include <exception>

using namespace kratos::service;

namespace kratos {
namespace loop {
namespace http {

// 每个工作线程一个独立的BoxNetwork指针
thread_local kratos::service::BoxNetwork *local_network_ = nullptr;
// 每个工作线程一个独立的HttpLoop指针
thread_local kratos::loop::HttpLoop *local_http_loop_ = nullptr;

} // namespace http
} // namespace loop
} // namespace kratos

// 输出系统错误日志
inline static auto print_sys_error(kratos::loop::HttpLoop *loop) -> void {
  if (loop->get_network()) {
    auto *appender = loop->get_network()->get_logger_appender();
    if (appender) {
      appender->write(klogger::Logger::WARNING, "[HttpLoop]%s",
                      kratos::util::get_last_errno_message().c_str());
    }
  }
}
/**
 * 发送HTTP RESPONSE.
 *
 * \param channel_ref 管道引用
 * \param error 错误信息
 * \return
 */
inline static auto return_http_error(kchannel_ref_t *channel_ref,
                                     const std::string &error) -> void {
  Json::Value root;
  root["_error"] = error;
  auto error_json = root.toStyledString();
  std::stringstream ss;
  ss << "HTTP/1.1 200 OK\r\n";
  ss << "Content-Length:" << error_json.size() << "\r\n";
  ss << "Access-Control-Allow-Headers: Content-Type, Access-Token" << "\r\n";
  ss << "Access-Control-Allow-Methods: POST,GET,OPTIONS" << "\r\n";
  ss << "Access-Control-Allow-Origin: *" << "\r\n";
  ss << "Content-Type: application/json" << "\r\n";
  ss << "\r\n";
  ss << error_json;
  auto *stream = knet_channel_ref_get_stream(channel_ref);
  auto http_proto = ss.str();
  // 发送HTTP协议
  knet_stream_push(stream, http_proto.c_str(), (int)http_proto.size());
  // 主动关闭
  knet_channel_ref_close(channel_ref);
}

/**
 * 发送HTTP RESPONSE for OPTIONS.
 *
 * \param channel_ref 管道引用
 * \return
 */
inline static auto return_http_access_control(kchannel_ref_t* channel_ref) -> void {
  std::stringstream ss;
  ss << "HTTP/1.1 200 OK\r\n";
  ss << "Access-Control-Allow-Origin: *" << "\r\n";
  ss << "Access-Control-Allow-Methods: POST,GET,OPTIONS" << "\r\n";
  ss << "Access-Control-Allow-Headers: Content-Type, Access-Token" << "\r\n";
  ss << "\r\n";
  auto* stream = knet_channel_ref_get_stream(channel_ref);
  auto http_proto = ss.str();
  // 发送HTTP协议
  knet_stream_push(stream, http_proto.c_str(), (int)http_proto.size());
  // 主动关闭
  knet_channel_ref_close(channel_ref);
}

/**
 * 答应FATAL日志.
 *
 * \param network BoxNetwork
 * \param error 错误描述
 * \return
 */
inline static auto log_fatal(kratos::service::BoxNetwork *network,
                             const std::string &error) -> void {
  auto *appender = network->get_logger_appender();
  if (appender) {
    appender->write(klogger::Logger::FATAL, error.c_str());
  }
}

/**
 * 网络底层内存分配.
 *
 * \param size 字节数
 * \return 内存地址
 */
static void *knet_malloc_func(size_t size) {
  if (!size) {
    return nullptr;
  }
  return box_malloc(size);
}
/**
 * 网络底层内存释放.
 *
 * \param p 内存地址
 */
static void knet_free_func(void *p) {
  if (p) {
    box_free(p);
  }
}

kratos::loop::HttpLoop::HttpLoop(service::BoxNetwork *network) {
  network_ = network;
}

kratos::loop::HttpLoop::~HttpLoop() {}

auto kratos::loop::HttpLoop::start() -> bool {
  if (!dynamic_cast<kratos::service::ServiceBox *>(network_)
           ->get_argument()
           .is_proxy()) {
    // 只有代理模式才能启动HTTP循环
    log_fatal(network_, "[HttpLoop]ONLY start on proxy mode");
    return false;
  }
  json_pb_ = std::make_unique<JsonPb>(network_);
  if (!json_pb_->load()) {
    return false;
  }
  // 替换网络库的内存分配器
  knet_set_malloc_func(knet_malloc_func);
  knet_set_free_func(knet_free_func);
  // 建立TCP网络循环
  loop_ = knet_loop_create();
  if (!loop_) {
    return false;
  }
  return true;
}

auto kratos::loop::HttpLoop::stop() -> bool {
  // 减少所有现存管道引用计数
  thread_channel_map_.clear();
  // 销毁网络循环
  if (loop_) {
    knet_loop_destroy(loop_);
    loop_ = nullptr;
  }
  return true;
}

auto kratos::loop::HttpLoop::worker_update() -> void {
  // 在网络线程内运行一次主循环
  if (loop_) {
    knet_loop_run_once(loop_);
  }
}

namespace kratos {
namespace loop {
namespace http {

// 网络线程内处理管道关闭事件
void do_channel_close(kchannel_ref_t *channel) {
  box_assert(local_network_, !channel);

  auto uuid = knet_channel_ref_get_uuid(channel);
  auto it = local_http_loop_->get_worker_channel_map().find(uuid);
  if (it != local_http_loop_->get_worker_channel_map().end()) {
    box_assert(local_network_, !it->second);

    // 在管道查询表内找到，释放资源
    knet_channel_ref_leave(it->second->channel);
    local_http_loop_->get_worker_channel_map().erase(it);
  }
  // 发送到逻辑线程
  NetEventData event_data{};
  event_data.event_id = NetEvent::close_notify;
  event_data.close_notify.channel_id = uuid;
  if (!local_network_->get_net_queue().send(event_data)) {
    // 发送失败，清理资源
    event_data.clear();
  }
}

// 网络线程内处理管道读事件
void do_channel_recv(kchannel_ref_t *channel) {
  box_assert(local_network_, !channel);

  auto uuid = knet_channel_ref_get_uuid(channel);
  auto *stream = knet_channel_ref_get_stream(channel);

  box_assert(local_network_, !stream);

  int size = knet_stream_available(stream);
  if (!size) {
    // 管道内没有数据却触发了读事件，关闭这个管道
    knet_channel_ref_close(channel);
    return;
  }
  auto it = local_http_loop_->get_worker_channel_map().find(uuid);
  if (it == local_http_loop_->get_worker_channel_map().end()) {
    // 读事件触发但没有找到管道
    knet_channel_ref_close(channel);
    return;
  }
  auto info_ptr = it->second;
  // 申请空间保存数据
  auto res = it->second->buffer->apply(size);
  if (!res.first || !res.second) {
    // 资源申请失败
    knet_channel_ref_close(channel);
    return;
  }
  // 读取所有数据
  if (error_ok != knet_stream_pop(stream, res.first, (int)res.second)) {
    knet_channel_ref_close(channel);
    return;
  }
  // 使用http_parser解析
  auto parsed_bytes =
      http_parser_execute(info_ptr->parser.get(), info_ptr->settings.get(),
                          res.first, (int)res.second);
  if (parsed_bytes != res.second) {
    knet_channel_ref_close(channel);
    return;
  }
}

// 网络线程内，由监听器创建的管道，事件处理函数
void client_cb(kchannel_ref_t *channel, knet_channel_cb_event_e e) {
  if (e & channel_cb_event_recv) {
    do_channel_recv(channel);
  } else if (e & channel_cb_event_close) {
    do_channel_close(channel);
  }
}

// 网络线程内，监听器接受了一个连接请求
void do_accept_channel(kchannel_ref_t *channel) {
  box_assert(local_network_, !channel);

  auto uuid = knet_channel_ref_get_uuid(channel);
  auto channel_shared = knet_channel_ref_share(channel);

  box_assert(local_network_, !channel_shared);

  // 接收到一个新连接即添加到管理表, TODO 读超时关闭, 保底定时器
  local_http_loop_->get_worker_channel_map()[uuid] =
      std::make_shared<kratos::loop::HttpLoop::ChannelInfo>(channel_shared);
  knet_channel_ref_set_cb(channel, client_cb);

  // 发送到逻辑线程
  NetEventData event_data{};
  event_data.event_id = NetEvent::accept_notify;
  event_data.accept_notify.channel_id = uuid;
  event_data.accept_notify.local_ip = nullptr;
  event_data.accept_notify.local_port = 0;
  event_data.accept_notify.peer_ip = nullptr;
  event_data.accept_notify.peer_port = 0;
  auto *uuid_ptr = knet_channel_ref_get_ptr(channel);

  box_assert(local_network_, !uuid_ptr);

  if (uuid_ptr) {
    auto listen_channel_uuid = *reinterpret_cast<std::uint64_t *>(uuid_ptr);
    auto &listener_map = local_network_->get_listener_name_map();
    auto it = listener_map.find(listen_channel_uuid);
    if (it != listener_map.end()) {
      const auto &listener_name = it->second.second;
      event_data.accept_notify.name = box_malloc(listener_name.size() + 1);
      box_assert(local_network_, !event_data.accept_notify.name);

      memcpy(event_data.accept_notify.name, listener_name.c_str(),
             listener_name.size() + 1);

      auto *local_addr = knet_channel_ref_get_local_address(channel);
      auto *peer_addr = knet_channel_ref_get_peer_address(channel);
      if (local_addr && address_get_ip(local_addr)) {
        event_data.accept_notify.local_ip =
            box_malloc(std::strlen(address_get_ip(local_addr)) + 1);
        box_assert(local_network_, !event_data.accept_notify.local_ip);
        memcpy(event_data.accept_notify.local_ip, address_get_ip(local_addr),
               std::strlen(address_get_ip(local_addr)) + 1);
        event_data.accept_notify.local_port = address_get_port(local_addr);
      }
      if (peer_addr && address_get_ip(peer_addr)) {
        event_data.accept_notify.peer_ip =
            box_malloc(std::strlen(address_get_ip(peer_addr)) + 1);
        box_assert(local_network_, !event_data.accept_notify.peer_ip);
        memcpy(event_data.accept_notify.peer_ip, address_get_ip(peer_addr),
               std::strlen(address_get_ip(peer_addr)) + 1);
        event_data.accept_notify.peer_port = address_get_port(peer_addr);
      }
    }
  }
  if (!uuid_ptr || !local_network_->get_net_queue().send(event_data)) {
    // 内部错误或发送失败, 销毁资源
    knet_channel_ref_close(channel_shared);
    event_data.clear();
  }
}

// 网络线程内，监听器事件处理函数
void acceptor_cb(kchannel_ref_t *channel, knet_channel_cb_event_e e) {
  if (e & channel_cb_event_accept) {
    do_accept_channel(channel);
  }
}

// 网络线程内，处理建立监听器事件
void do_listen_request(const NetEventData &request, kloop_t *loop,
                       kratos::loop::HttpLoop *http_loop) {
  auto *channel = knet_loop_create_channel(
      loop,
      0, // 已废弃
      http_loop->get_network()->get_channel_recv_buffer_len());
  box_assert(http_loop->get_network(), !channel);

  auto uuid = knet_channel_ref_get_uuid(channel);
  box_assert(http_loop->get_network(), !uuid);

  auto retval = knet_channel_ref_accept(channel, request.listen_request.host,
                                        request.listen_request.port,
                                        512 // TODO configurable
  );

  box_assert(http_loop->get_network(), !request.listen_request.name);
  std::string listener_name(request.listen_request.name);

  NetEventData response{};
  auto length = static_cast<int>(std::strlen(request.listen_request.name) + 1);
  response.listen_response.name = box_malloc(length);
  box_assert(http_loop->get_network(), !response.listen_response.name);

  memcpy(response.listen_response.name, request.listen_request.name, length);
  response.event_id = NetEvent::listen_response;
  if (error_ok != retval) {
    response.listen_response.channel_id = 0;
    response.listen_response.success = false;
    print_sys_error(http_loop);
    knet_channel_ref_close(channel);
  } else {
    auto *shared_channel = knet_channel_ref_share(channel);
    box_assert(http_loop->get_network(), !shared_channel);

    http_loop->get_worker_channel_map()[uuid] =
        std::make_shared<kratos::loop::HttpLoop::ChannelInfo>(shared_channel);
    response.listen_response.channel_id = uuid;
    response.listen_response.success = true;
    knet_channel_ref_set_cb(channel, acceptor_cb);
  }
  if (!http_loop->get_network()->get_net_queue().send(response)) {
    // 发送失败，清理资源
    // 发生这种情况，会导致逻辑线程无法得知监听器是否启动成功
    response.clear();
    knet_channel_ref_close(channel);
  } else {
    if (error_ok == retval) {
      http_loop->get_network()->get_listener_name_map()[uuid] = {uuid,
                                                                 listener_name};
      auto *uuid_ptr =
          &(http_loop->get_network()->get_listener_name_map()[uuid].first);
      knet_channel_ref_set_ptr(channel, reinterpret_cast<void *>(uuid_ptr));
    }
  }
}

// 网络线程内，处理发送数据事件
void do_send_request(const NetEventData &request, kloop_t * /*loop*/,
                     kratos::loop::HttpLoop *http_loop) {
  box_assert(http_loop->get_network(), !request.send_data_notify.data_ptr);

  auto &worker_channel_map = http_loop->get_worker_channel_map();
  auto it = worker_channel_map.find(request.send_data_notify.channel_id);
  if (it == worker_channel_map.end()) {
    return;
  }
  //
  // 读取PB, PB->JSON, 构造HTTP协议并发送, 关闭管道
  //
  auto *header =
      reinterpret_cast<rpc::RpcMsgHeader *>(request.send_data_notify.data_ptr);
  if (ntohl(header->type) != (rpc::MsgTypeID)rpc::RpcMsgType::RPC_RETURN) {
    // 只支持RPC_RETURN
    knet_channel_ref_close(it->second->channel);
    return;
  }
  auto *channel_ref = it->second->channel;
  auto *return_header =
      reinterpret_cast<rpc::RpcRetHeader *>(request.send_data_notify.data_ptr);
  return_header->ntoh();
  Json::Value root;
  if (return_header->retHeader.errorID == rpc::ErrorID(rpc::RpcError::OK)) {
    try {
      if (!http_loop->get_json_pb()->to_json(
              it->second->uuid, it->second->method_id, root,
              request.send_data_notify.data_ptr + sizeof(rpc::RpcRetHeader),
              return_header->header.length - sizeof(rpc::RpcRetHeader))) {
        knet_channel_ref_close(it->second->channel);
        return;
      }
      // 构造HTTP协议并发送
      std::stringstream ss;
      auto content = root.toStyledString();
      ss << "HTTP/1.1 200 OK\r\n";
      ss << "Content-Length:" << content.size() << "\r\n";
      ss << "Content-Type:" << "application/json" << "\r\n";
      ss << "Access-Control-Allow-Origin: *" << "\r\n";
      ss << "Access-Control-Allow-Methods: POST,GET,OPTIONS" << "\r\n";
      ss << "Access-Control-Allow-Headers: Content-Type, Access-Token" << "\r\n";
      ss << "\r\n";
      ss << root.toStyledString();
      auto *stream = knet_channel_ref_get_stream(channel_ref);
      box_assert(http_loop->get_network(), !stream);
      auto http_proto = ss.str();
      // 发送HTTP协议
      auto retval =
          knet_stream_push(stream, http_proto.c_str(), (int)http_proto.size());
      if (error_ok != retval) {
        print_sys_error(http_loop);
      }
      // 主动关闭
      knet_channel_ref_close(channel_ref);
    } catch (std::exception &ex) {
      return_http_error(it->second->channel, ex.what());
      return;
    }
  } else {
    switch (rpc::RpcError(return_header->retHeader.errorID)) {
    case rpc::RpcError::NOT_FOUND:
      return_http_error(channel_ref, "Service not found");
      break;
    case rpc::RpcError::EXCEPTION:
      return_http_error(channel_ref, "Service internal exception");
      break;
    case rpc::RpcError::TIMEOUT:
      return_http_error(channel_ref, "Service timeout");
      break;
    case rpc::RpcError::LIMIT:
      return_http_error(channel_ref, "Service call reach limit");
      break;
    default:
      break;
    }
  }
}

// 网络线程内，处理管道关闭事件
void do_close_request(const NetEventData &request, kloop_t * /*loop*/,
                      kratos::loop::HttpLoop *http_loop) {
  auto uuid = request.close_request.channel_id;
  auto it = http_loop->get_worker_channel_map().find(uuid);
  if (it != http_loop->get_worker_channel_map().end()) {
    knet_channel_ref_close(it->second->channel);
  }
}

} // namespace http
} // namespace loop
} // namespace kratos

auto kratos::loop::HttpLoop::do_worker_event(
    const service::NetEventData &event_data) -> void {
  if (!http::local_network_) {
    http::local_network_ = network_;
  }
  if (!http::local_http_loop_) {
    http::local_http_loop_ = this;
  }
  if (!loop_) {
    return;
  }
  switch (event_data.event_id) {
  case NetEvent::listen_request: {
    http::do_listen_request(event_data, loop_, this);
  } break;
  case NetEvent::connect_request: {
    if (network_) {
      auto *appender = get_network()->get_logger_appender();
      if (appender) {
        appender->write(klogger::Logger::WARNING,
                        "[HttpLoop]HTTP connector not implements");
      }
    }
  } break;
  case NetEvent::send_data_notify: {
    http::do_send_request(event_data, loop_, this);
  } break;
  case NetEvent::close_request: {
    http::do_close_request(event_data, loop_, this);
  } break;
  default:
    break;
  }
}

auto kratos::loop::HttpLoop::get_network() -> service::BoxNetwork * {
  return network_;
}

kratos::loop::HttpLoop::ChannelMap &
kratos::loop::HttpLoop::get_worker_channel_map() noexcept {
  return thread_channel_map_;
}

auto kratos::loop::HttpLoop::get_json_pb() -> JsonPb * {
  return json_pb_.get();
}

int kratos::loop::HttpLoop::on_message_begin(http_parser *) { return 0; }

int kratos::loop::HttpLoop::on_message_complete(http_parser *parser) {
  static rpc::CallID call_id{1};
  auto* info_ptr = reinterpret_cast<ChannelInfo*>(parser->data);
  if (parser->method == HTTP_OPTIONS) {
    return_http_access_control(info_ptr->channel);
    return 0;
  }
  //
  // JSON -> PB, 构造RPC协议头+协议体
  //
  Json::CharReaderBuilder builder;
  std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
  Json::Value root;
  const auto *start = info_ptr->body.c_str();
  const auto *end = info_ptr->body.c_str() + info_ptr->body.size();
  Json::String error;
  if (!reader->parse(start, end, &root, &error)) {
    return_http_error(info_ptr->channel, "Invalid JSON format, error:" + error);
    knet_channel_ref_close(info_ptr->channel);
    return 0;
  }
  if (!root.isMember("args")) {
    return_http_error(info_ptr->channel, "Need 'args' attribute");
    return 0;
  }
  std::string service_name;
  std::string method_name;
  if (!root.isMember("service") || !root.isMember("method")) {
    std::vector<std::string> result;
    util::split(info_ptr->uri, "/", result);
    if (result.size() < 2) {
      return_http_error(info_ptr->channel, "Invalid API");
      return 0;
    }
    method_name = result.back();
    service_name = *(result.rbegin() + 1);
  } else {
    service_name = root["service"].asString();
    method_name = root["method"].asString();
  }
  auto method_info = http::local_http_loop_->get_json_pb()->get_method_info(
      service_name, method_name);
  if (!method_info.first || !method_info.second) {
    return_http_error(info_ptr->channel, "Invalid JSON format");
    return 0;
  }
  // 设置UUID, method ID
  info_ptr->uuid = method_info.first;
  info_ptr->method_id = method_info.second;
  auto* method_type_ptr = http::local_http_loop_->get_json_pb()->get_method_type(method_info.first,
    method_info.second);
  if (!method_type_ptr) {
    return_http_error(info_ptr->channel, "Service or method not found");
    return 0;
  }
  char *rpc_proto{nullptr};
  std::size_t length{0};
  try {
    if (!http::local_http_loop_->get_json_pb()->to_rpc(root, rpc_proto,
                                                       length, method_type_ptr)) {
      return 0;
    }
  } catch (std::exception &ex) {
    return_http_error(info_ptr->channel, ex.what());
    return 0;
  }
  rpc::RpcCallHeader call_header;
  auto total_length = sizeof(call_header) + length;
  call_header.header.type = rpc::MsgTypeID(rpc::RpcMsgType::RPC_CALL);
  call_header.header.length = std::uint32_t(total_length);
  call_header.callHeader.callID = call_id++;
  call_header.callHeader.serviceID = 0; // TODO HTTP暂时只支持无状态的调用方式
  call_header.callHeader.methodID = method_info.second;
  call_header.callHeader.serviceUUID = method_info.first;
  call_header.hton();
  // send to MAIN THREAD
  auto uuid = knet_channel_ref_get_uuid(info_ptr->channel);
  NetEventData event_data{};
  event_data.event_id = NetEvent::recv_data_notify;
  event_data.recv_data_notify.channel_id = uuid;
  event_data.recv_data_notify.data_ptr = box_malloc(total_length);
  event_data.recv_data_notify.length = int(total_length);
  std::memcpy(event_data.recv_data_notify.data_ptr, &call_header,
              sizeof(call_header));
  std::memcpy(event_data.recv_data_notify.data_ptr + sizeof(call_header),
              rpc_proto, length);
  // 发送到逻辑线程, 模拟一个RPC_CALL
  if (!http::local_network_->get_net_queue().send(event_data)) {
    event_data.clear();
    // 发送失败，清理资源
    knet_channel_ref_close(info_ptr->channel);
  } else {
    if (http::local_http_loop_->get_json_pb()->is_oneway(info_ptr->uuid,
                                                         info_ptr->method_id)) {
      // oneway方法没有返回值
      return_http_error(info_ptr->channel, "oneway method is not supported");
      knet_channel_ref_close(info_ptr->channel);
      return 0;
    }
  }
  return 0;
}

int kratos::loop::HttpLoop::on_url(http_parser *parser, const char *at,
                                   std::size_t length) {
  auto *info_ptr = reinterpret_cast<ChannelInfo *>(parser->data);
  info_ptr->uri = std::string(at, length);
  return 0;
}

int kratos::loop::HttpLoop::on_header_field(http_parser *parser, const char *at,
                                            std::size_t length) {
  auto *info_ptr = reinterpret_cast<ChannelInfo *>(parser->data);
  info_ptr->last_key = std::string(at, length);
  return 0;
}

int kratos::loop::HttpLoop::on_header_value(http_parser *parser, const char *at,
                                            std::size_t length) {
  auto *info_ptr = reinterpret_cast<ChannelInfo *>(parser->data);
  info_ptr->header[info_ptr->last_key] = std::string(at, length);
  return 0;
}

int kratos::loop::HttpLoop::on_status(http_parser *, const char *,
                                      std::size_t) {
  return 0;
}

int kratos::loop::HttpLoop::on_headers_complete(http_parser *) { return 0; }

int kratos::loop::HttpLoop::on_body(http_parser *parser, const char *at,
                                    std::size_t length) {
  auto *info_ptr = reinterpret_cast<ChannelInfo *>(parser->data);
  info_ptr->body.append(at, length);
  return 0;
}

int kratos::loop::HttpLoop::on_chunk_header(http_parser *) { return 0; }

int kratos::loop::HttpLoop::on_chunk_complete(http_parser *) { return 0; }

inline kratos::loop::HttpLoop::ChannelInfo::~ChannelInfo() {}

kratos::loop::HttpLoop::ChannelInfo::ChannelInfo(
    kchannel_ref_t *channel_shared) {
  channel = channel_shared;
  parser = std::make_unique<http_parser>();
  settings = std::make_unique<http_parser_settings>();
  http_parser_init(parser.get(), HTTP_REQUEST);
  http_parser_settings_init(settings.get());
  buffer = std::make_unique<kratos::util::WriteBuffer>();
  parser->data = this;
  settings->on_body = &HttpLoop::on_body;
  settings->on_chunk_complete = &HttpLoop::on_chunk_complete;
  settings->on_chunk_header = &HttpLoop::on_chunk_header;
  settings->on_headers_complete = &HttpLoop::on_headers_complete;
  settings->on_header_field = &HttpLoop::on_header_field;
  settings->on_header_value = &HttpLoop::on_header_value;
  settings->on_message_begin = &HttpLoop::on_message_begin;
  settings->on_message_complete = &HttpLoop::on_message_complete;
  settings->on_status = &HttpLoop::on_status;
  settings->on_url = &HttpLoop::on_url;
}
