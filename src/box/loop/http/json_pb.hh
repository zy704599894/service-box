#pragma once

#include <memory>
#include <string>

#include "util/method_type.hh"
#include "util/write_buffer.hh"

namespace kratos {
namespace service {
class BoxNetwork;
}
} // namespace kratos

namespace kratos {
namespace util {
class WriteBuffer;
}
} // namespace kratos

namespace Json {
class Value;
}

namespace kratos {
namespace loop {

/**
 * JSON与protobuf::Message之间的转换工具.
 */
class JsonPb {
  std::unique_ptr<kratos::util::MsgFactory> msg_factory_ptr_; ///< 消息工厂
  kratos::service::BoxNetwork *network_{nullptr};             ///< 容器
  kratos::util::WriteBuffer buffer_;                          ///< 写入缓存

public:
  /**
   * 构造.
   *
   * \param network BoxNetwork
   */
  JsonPb(kratos::service::BoxNetwork *network);
  /**
   * 析构.
   *
   */
  ~JsonPb();
  /**
   * 加载配置并初始化资源.
   *
   * \return true or false
   */
  auto load() noexcept(false) -> bool;
  /**
   * 将JSON串转化为protobuf::Message对象并序列化到缓冲区.
   *
   * \param value JSON对象
   * \param [IN OUT] rpc_proto 缓冲区地址, 此地址不能保存
   * \param [IN OUT] length 缓冲区长度, 此地址长度不能保存
   * \param method_type_ptr MethodType指针
   * \return true or false
   */
  auto to_rpc(Json::Value &value, char *&rpc_proto, std::size_t &length,
              const util::MethodType *method_type_ptr) noexcept(false) -> bool;
  /**
   * 将protobuf::Message数据流转化为一个JSON对象.
   *
   * \param uuid 服务UUID
   * \param method_id 方法ID
   * \param [IN OUT] value JSON对象
   * \param rpc_proto protobuf::Message数据流
   * \param length protobuf::Message数据流长度
   * \return true or false
   */
  auto to_json(rpc::ServiceUUID uuid, rpc::MethodID method_id,
               Json::Value &value, const char *rpc_proto,
               std::size_t length) noexcept(false) -> bool;
  /**
   * 获取{rpc::ServiceUUID, rpc::MethodID}.
   *
   * \param service_name 服务名
   * \param method_name 方法名
   * \return {rpc::ServiceUUID, rpc::MethodID}
   */
  auto get_method_info(const std::string &service_name,
                       const std::string &method_name)
      -> std::pair<rpc::ServiceUUID, rpc::MethodID>;
  /**
   * 获取是否是oneway方法
   *
   * \param uuid 服务UUID
   * \param method_id 方法ID
   * \return true or false
   */
  auto is_oneway(rpc::ServiceUUID uuid, rpc::MethodID method_id) -> bool;

  /**
   * 获取方法原型信息
   *
   * \param uuid 服务UUID
   * \param method_id 方法ID
   * \return MethodType指针
   */
  auto get_method_type(rpc::ServiceUUID uuid, rpc::MethodID method_id)
      -> const util::MethodType *;

private:
  /**
   * 将JSON对象转换为ProtobufMessage.
   *
   * \param value JSON对象
   * \param msg ProtobufMessage
   * \return true or false
   */
  auto encode(const Json::Value &value, ProtobufMessage *msg) -> bool;
  /**
   * 将ProtobufMessage转换为JSON对象.
   *
   * \param pb_msg ProtobufMessage
   * \return JSON对象
   */
  auto decode(const ProtobufMessage &pb_msg) -> std::unique_ptr<Json::Value>;
  //
  // 以下为JSON和Protobuf间的转换函数
  //
  auto on_field(const ProtobufMessage &msg,
                const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_map_field(const ProtobufMessage &msg,
                    const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_map_field_key(const ProtobufMessage &msg,
                        const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_map_field_value(const ProtobufMessage &msg,
                          const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_repeated_field(const ProtobufMessage &msg,
                         const ProtobufFieldDescriptor *field)
      -> std::unique_ptr<Json::Value>;
  auto on_object_value(const Json::Value &obj_ptr, ProtobufMessage &msg,
                       const ProtobufReflection *ref,
                       const ProtobufFieldDescriptor *field) -> void;
  auto on_object_map(const Json::Value &obj_ptr, ProtobufMessage &msg,
                     const ProtobufReflection *ref,
                     const ProtobufFieldDescriptor *field) -> void;
  auto on_object_repeated_value(const Json::Value &obj_ptr,
                                ProtobufMessage &msg,
                                const ProtobufReflection *ref,
                                const ProtobufFieldDescriptor *field) -> void;
};

} // namespace loop
} // namespace kratos
