#pragma once

#include <cstdlib>
#if (defined(_WIN32) || defined(_WIN64))
#include <WinSock2.h>
#include <cstdint>
using SocketType = SOCKET;
using SocketAddress = struct sockaddr_in;
#else
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
using SocketType = int;
using SocketAddress = struct sockaddr_in;
constexpr static int INVALID_SOCKET = -1;
#endif // (defined(_WIN32) || defined(_WIN64))

enum class UdpType {
  Unknown,
  Acceptor = 1,
  Connector,
  Remote,
};

static constexpr std::size_t NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE =
    1024 * 1024;

// 每次发送的UDP包最大长度
static constexpr std::size_t NETWORK_IOLOOP_UDP_PACKET_MAX_SIZE = 548;
