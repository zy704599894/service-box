#pragma once

#include "udp_defines.hh"

#include <array>
#include <ctime>
#include <functional>
#include <memory>
#include <string>

namespace kratos {
namespace network {

class UdpSocket;
class UdpChannel;
class UdpRemoteChannel;

using AcceptFunction = std::function<void(std::uint64_t, const std::string &)>;
using ReceiveFunction =
    std::function<void(std::uint64_t, const char *, std::uint32_t)>;

/// UDP管道事件循环
class UdpChannelLoop {
  using ChannelMap = std::unordered_map<std::uint64_t, UdpChannel *>;
  ChannelMap channelMap_; // 管道表
  std::unique_ptr<char[]> buffer_{
      std::make_unique<char[]>(NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE)};
  using ListenerNameMap = std::unordered_map<std::uint64_t, std::string>;
  ListenerNameMap listener_name_map_;

public:
  // 构造
  UdpChannelLoop();
  // 析构
  ~UdpChannelLoop();
  // 处理事件
  void update(AcceptFunction accepted, ReceiveFunction received);
  // 建立新的监听器(模拟)
  // @param ip IP
  // @param port 端口
  std::uint64_t newAcceptor(const std::string &ip, std::uint16_t port,
                            const std::string &name, std::size_t netBufSize);
  // 建立新的连接器(模拟)
  // @param ip IP
  // @param port 端口
  std::uint64_t newConnector(const std::string &ip, std::uint16_t port,
                             std::size_t netBufSize);
  // 发送
  // @param id 管道ID
  // @param data 数据指针
  // @param length 数据长度
  void send(std::uint64_t id, const char *data, std::size_t length);
  // 关闭
  // @param id 管道ID
  void close(std::uint64_t id);
  // 检测管道是否存在
  bool exists(std::uint64_t id);

private:
  // 处理Acceptor读事件
  // @param channel 读管道
  void _acceptorReceive(UdpChannel *channel, AcceptFunction accepted,
                        ReceiveFunction received);
  // 处理Connector读事件
  // @param channel 读管道
  void _connectorReceive(UdpChannel *channel, ReceiveFunction received);
};

} // namespace network
} // namespace kratos
