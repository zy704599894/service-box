#pragma once

#include <memory>

namespace kratos {
namespace config {
class BoxConfig;
}
} // namespace kratos

namespace kratos {
namespace service {

class CallTracer;

/**
 * 调用记录存储类型.
 */
enum class CallTracerStoreType {
  KAFKA = 1,   ///< KAFKA
  FILE_ZIPKIN, ///< ZIPKIN日志文件
  ZIPKIN,      ///< ZIPKIN
};

using CallTracerPtr = std::shared_ptr<CallTracer>;

/**
 * 调用记录器工厂.
 */
class CallTracerFactory {
public:
  virtual ~CallTracerFactory() {}
  /**
   * 通过类型创建记录器.
   *
   * \param store_type 记录存储类型
   * \param config 配置
   * \return 记录器智能指针
   */
  virtual auto create_from(CallTracerStoreType store_type,
                           kratos::config::BoxConfig &config)
      -> CallTracerPtr = 0;
  /**
   * 通过类型名创建记录器.
   *
   * \param config 配置
   * \return 记录器智能指针
   */
  virtual auto create_from(kratos::config::BoxConfig &config)
      -> CallTracerPtr = 0;
};

/**
 * 获取调用记录器工厂指针.
 */
extern auto get_call_tracer_factory() -> CallTracerFactory *;

} // namespace service
} // namespace kratos
