#pragma once

#include <cstdint>
#include <google/protobuf/compiler/importer.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/message.h>
#include <google/protobuf/reflection.h>
#include <string>

#ifdef GetMessage
#undef GetMessage
#endif

namespace kratos {

/**
 * 设置调用信息.
 *
 * @param msg 消息
 * @param [OUT] trace_id Trace ID
 * @param [OUT] span_id Span ID
 * @param [OUT] parent_span_id Parent span ID
 * @return true, false
 */
static inline auto get_trace_info(google::protobuf::Message &msg,
                           std::string &trace_id, std::uint64_t &span_id,
                           std::uint64_t &parent_span_id) -> bool {
  const auto *reflection = msg.GetReflection();
  const auto *descriptor = msg.GetDescriptor();
  const auto *trace_descriptor = descriptor->FindFieldByName("trace_info");
  if (!trace_descriptor || !reflection || !trace_descriptor) {
    return false;
  }
  const auto &trace_msg = reflection->GetMessage(msg, trace_descriptor);
  const auto *trace_reflection = trace_msg.GetReflection();
  const auto *trace_id_descriptor = trace_msg.GetDescriptor()->field(0);
  if (!trace_reflection->HasField(trace_msg, trace_id_descriptor)) {
    //
    // 没有设置trace_id字段
    //
    return false;
  }
  //
  // 获取并设置trace_id, span_id, parent_span_id
  //
  trace_id = trace_reflection->GetString(trace_msg,
                                         trace_msg.GetDescriptor()->field(0));
  span_id = trace_reflection->GetUInt64(trace_msg,
                                        trace_msg.GetDescriptor()->field(1));
  parent_span_id = trace_reflection->GetUInt64(
      trace_msg, trace_msg.GetDescriptor()->field(2));
  return true;
}
/**
 * 获取调用信息.
 * @param msg 消息
 * @param trace_id Trace ID
 * @param span_id Span ID
 * @param parent_span_id Parent span ID
 * @return true, false
 */
static inline auto set_trace_info(google::protobuf::Message &msg,
                           const std::string &trace_id, std::uint64_t span_id,
                           std::uint64_t parent_span_id) -> bool {
  const auto *reflection = msg.GetReflection();
  const auto *descriptor = msg.GetDescriptor();
  const auto *trace_descriptor = descriptor->FindFieldByName("trace_info");
  if (!trace_descriptor || !reflection || !trace_descriptor) {
    return false;
  }
  auto *trace_msg = reflection->MutableMessage(&msg, trace_descriptor);
  const auto *trace_reflection = trace_msg->GetReflection();
  //
  // 设置trace_id, span_id, parent_span_id
  //
  trace_reflection->SetString(trace_msg, trace_msg->GetDescriptor()->field(0),
                              trace_id);
  trace_reflection->SetUInt64(trace_msg, trace_msg->GetDescriptor()->field(1),
                              span_id);
  trace_reflection->SetUInt64(trace_msg, trace_msg->GetDescriptor()->field(2),
                              parent_span_id);
  return true;
}

} // namespace kratos
