﻿#pragma once

#include <string>
#include <vector>

namespace kratos {
namespace argument {

/**
 * 命令行配置器接口
 *
 * 获取启动时的命令行参数
 */
class BoxArgument {
public:
  // 默认配置文件名
  constexpr static const char *DEFAULT_CONFIG_FILE_NAME = "config.default.cfg";
  using PositinalList = std::vector<std::string>;

public:
  /**
   * 析构
   */
  virtual ~BoxArgument() {}
  /**
   * 取得容器启动所使用的配置文件路径.
   *
   * \return 配置文件路径
   */
  virtual auto get_config_file_path() const noexcept -> const std::string & = 0;
  /**
   * 获取配置文件名.
   *
   * \return 配置文件名
   */
  virtual auto get_config_file_name() const noexcept -> std::string = 0;
  /**
   * 获取最大帧率(帧/秒).
   *
   * 容器将按照这个帧率来运行逻辑主循环
   *
   * \return 最大帧率(帧/秒)
   */
  virtual auto get_max_frame() const noexcept -> int = 0;
  /**
   * 是否打印help
   */
  virtual auto is_print_help() const noexcept -> bool = 0;
  /**
   * 获取帮助信息
   */
  virtual auto get_help_string() const noexcept -> std::string = 0;
  /**
   * 获取启动命令.
   *
   * \return
   */
  virtual auto get_command() const noexcept -> const std::string & = 0;
  /**
   * 获取远程服务容器地址.
   *
   * \return
   */
  virtual auto get_host() const noexcept -> const std::string & = 0;
  /**
   * 是否已daemon方式启动，优先级大于配置.
   *
   * \return
   */
  virtual auto is_daemon() const noexcept -> bool = 0;
  /**
   * 获取注册到操作系统的服务名称.
   *
   * \return
   */
  virtual auto get_service_name() const noexcept -> const std::string & = 0;
  /**
   * 获取远程配置中心API，下载配置.
   *
   * \return 配置中心API
   */
  virtual auto get_config_center_api_url() const noexcept
      -> const std::string & = 0;
  /**
   * 是否开启系统异常捕获，并尝试继续运行
   *
   * \return true或false
   */
  virtual auto is_open_system_exception() const noexcept -> bool = 0;
  /**
   * 是否是proxy.
   *
   * \return true或false
   */
  virtual auto is_proxy() const noexcept -> bool = 0;
  /**
   * 获取需要连接的代理host.
   *
   * \return
   */
  virtual auto get_proxy_host() const noexcept -> std::string = 0;
  /**
   * 获取用户定义的参数
   */
  virtual auto get_user_options() const noexcept -> PositinalList = 0;
};

} // namespace argument
} // namespace kratos
