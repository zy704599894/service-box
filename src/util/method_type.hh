#pragma once

#include <memory>
#include <string>

#include "root/rpc_root.h"
#include "util/box_std_allocator.hh"
#include "util/object_pool.hh"

namespace kratos {
namespace service {
class BoxNetwork;
}
} // namespace kratos

namespace Json {
class Value;
}

namespace google {
namespace protobuf {
class Reflection;
class Message;
class Descriptor;
class FieldDescriptor;
class FileDescriptor;
class DynamicMessageFactory;
class Arena;
namespace compiler {
class Importer;
class DiskSourceTree;
} // namespace compiler
} // namespace protobuf
} // namespace google

using ProtobufReflection = google::protobuf::Reflection;
using ProtobufMessage = google::protobuf::Message;
using ProtobufFieldDescriptor = google::protobuf::FieldDescriptor;
using ProtobufImporter = google::protobuf::compiler::Importer;
using ProtobufFileDescriptor = google::protobuf::FileDescriptor;
using ProtobufDescriptor = google::protobuf::Descriptor;
using ProtobufDynamicMessageFactory = google::protobuf::DynamicMessageFactory;
using ProtobufArena = google::protobuf::Arena;
using ProtobufDiskSourceTree = google::protobuf::compiler::DiskSourceTree;
using ProtobufMessagePtr = std::shared_ptr<ProtobufMessage>;

namespace kratos {
namespace util {

/**
 * 方法调用原型
 */
struct MethodType {
  struct NamePair {
    std::string pb_name;
    std::string decl_name;
  };
  using ArgNameVector = std::vector<NamePair>;
  const ProtobufDescriptor *request_message_descriptor{nullptr}; ///< 调用参数
  const ProtobufDescriptor *response_message_descriptor{nullptr}; ///< 返回值
  bool oneway{false};                           ///< 是否是oneway方法
  std::string service_name;                     ///< 服务名
  std::string method_name;                      ///< 方法名
  int timeout;                                  ///< 调用的timeout
  ProtobufMessagePtr request_message{nullptr};  ///< 参数
  ProtobufMessagePtr response_message{nullptr}; ///< 返回值
  std::string uuid_string;                      ///< 服务UUID
  std::string lua_real_method_name;             ///< Lua内方法名
  bool has_ret_value;                           ///< 是否有返回值
  ArgNameVector arg_name_vector;                ///< 参数名字数组
  MethodType(const ProtobufDescriptor *request_descriptor,
             const ProtobufDescriptor *response_descriptor, bool is_oneway,
             const std::string &service_name,
             const std::string &service_method_name, int call_timeout,
             ProtobufMessage *request, ProtobufMessage *response,
             const std::string &uuid, bool has_ret_value) noexcept;
  MethodType(const MethodType &rht) noexcept;
  MethodType(MethodType &&rht) noexcept;
  const MethodType &operator=(const MethodType &) = delete;
  /**
   * 有效性检测
   *
   * \return true或false
   */
  operator bool() { return !method_name.empty(); }
  /**
   * 检测是否有返回值
   *
   * \return true或false
   */
  bool has_retval() const;
};

/**
 * 协议工厂
 */
class MsgFactory {
  using MessageFactoryVector = kratos::service::PoolVector<MethodType>;
  kratos::unique_pool_ptr<ProtobufDynamicMessageFactory> factory_{
      nullptr}; ///< 协议动态工厂
  using MessageFactoryMap =
      kratos::service::PoolUnorederedMap<rpc::ServiceUUID,
                                         MessageFactoryVector>;
  MessageFactoryMap msg_factory_map_; ///< 协议工厂
  kratos::unique_pool_ptr<ProtobufImporter> importer_{nullptr}; ///< 加载器
  kratos::service::BoxNetwork *network_{nullptr};               ///< 网络
  using MethodTable = kratos::service::PoolUnorederedMap<
      std::string, std::pair<rpc::ServiceUUID, rpc::MethodID>>;
  MethodTable method_table_; ///< {method name, {service uuid, method ID}}

public:
  /**
   * 构造
   */
  MsgFactory(kratos::service::BoxNetwork *network);
  /**
   * 析构
   */
  ~MsgFactory();
  /**
   * 加载
   *
   * \param idl_json_file IDL JSON文件
   * \param idl_proto_root_dir IDL所对应的.proto文件的根目录
   * \param file_name 服务所在的.proto文件名
   * \return true或false
   */
  auto load(const std::string &idl_json_file,
            const std::string &idl_proto_root_dir, const std::string &file_name)
      -> bool;
  /**
   * 加载idl_proto_root_dir内所有的.proto文件
   *
   * \param idl_json_root_dir IDL JSON文件根目录
   * \param idl_proto_root_dir IDL所对应的.proto文件的根目录
   * \return true或false
   */
  auto load(const std::string &idl_json_root_dir,
            const std::string &idl_proto_root_dir) -> bool;
  /**
   * 建立方法的调用参数
   *
   * \param service_uuid 服务UUID
   * \param method_id 方法ID
   * \return 调用参数类
   */
  auto new_call_param(rpc::ServiceUUID service_uuid,
                      rpc::MethodID method_id) noexcept -> ProtobufMessage *;
  /**
   * 建立方法的调用返回
   *
   * \param service_uuid 服务UUID
   * \param method_id 方法ID
   * \return 调用返回类
   */
  auto new_call_return(rpc::ServiceUUID service_uuid,
                       rpc::MethodID method_id) noexcept -> ProtobufMessage *;
  /**
   * 建立方法的调用参数
   *
   * \param service_name 服务名
   * \param method_name 方法名
   * \return 调用参数类
   */
  auto new_call_param(const std::string &service_name,
                      const std::string &method_name) noexcept
      -> ProtobufMessage *;
  /**
   * 建立方法的调用返回
   *
   * \param service_name 服务名
   * \param method_name 方法名
   * \return 调用返回类
   */
  auto new_call_return(const std::string &service_name,
                       const std::string &method_name) noexcept
      -> ProtobufMessage *;
  /**
   * 获取{service UUID, method ID}
   *
   * \param service_name 服务名
   * \param method_name 方法名
   * \return {service UUID, method ID}
   */
  auto get_method_info(const std::string &service_name,
                       const std::string &method_name)
      -> std::pair<rpc::ServiceUUID, rpc::MethodID>;
  /**
   * 获取调用类型
   *
   * \param service_uuid 服务UUID
   * \param method_id 方法ID
   * \return 调用类型
   */
  auto get_type(rpc::ServiceUUID service_uuid,
                rpc::MethodID method_id) const noexcept -> const MethodType *;

private:
  /**
   * 加载
   *
   * \param idl_json_root JSON根节点
   * \param file_descriptor FileDescriptor
   * \return true或false
   */
  auto load(const Json::Value &idl_json_root,
            const ProtobufFileDescriptor *file_descriptor) -> bool;
};

} // namespace util
} // namespace kratos
