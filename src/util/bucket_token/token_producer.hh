#pragma once

#include <cstdint>
#include <ctime>

#include "bucket_token_defines.hh"

namespace kratos {
namespace util {

/**
 * 令牌产生器.
 */
class TokenProducer {
public:
  virtual ~TokenProducer() {}
  /**
   * 设置每秒产生的令牌数量.
   * 
   * \param count 每秒产生的令牌数量
   * \return 
   */
  virtual auto set_rate_per_second(std::size_t count) -> void = 0;
  /**
   * 消耗一个令牌.
   *
   * \retval true 成功
   * \retval false 令牌耗尽
   */
  virtual auto consume() -> bool = 0;
  /**
   * 主循环, 产生令牌.
   *
   * \param now 当前时间戳，毫秒
   * \return 当前产生的令牌数量
   */
  virtual auto update(std::time_t now) -> std::size_t = 0;
  /**
   * 获取当前桶内令牌数量.
   *
   * \return 当前桶内令牌数量
   */
  virtual auto token_count() -> std::size_t = 0;
  /**
   * 设置桶容量.
   * 
   * \param bucket_volume 桶容量
   * \return 
   */
  virtual auto set_volume(std::size_t bucket_volume) -> void = 0;
};

/**
 * 令牌产生器工厂.
 */
class TokenProducerFactory {
public:
  virtual ~TokenProducerFactory() {}
  /**
   * 建立一个令牌产生器.
   *
   * \return 令牌产生器
   */
  virtual auto create_producer() -> TokenProducerPtr = 0;
};

} // namespace util
} // namespace kratos
