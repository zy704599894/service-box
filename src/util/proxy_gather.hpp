﻿#pragma once

#include "box/service_context.hh"
#include "root/coroutine/coroutine.h"
#include "root/rpc_interface.h"

#include <any>
#include <chrono>
#include <cstdint>
#include <functional>
#include <stdexcept>
#include <unordered_map>
#include <vector>

namespace kratos {
namespace util {

/**
 * 发起批量协程调用, 并等待所有调用完成并返回批量结果
 *
 * 调用将在独立的协程内运行，ProxyGather将启动多个协程并发（协程调度）的执行并搜集返回结果，不保证执行顺序
 * 但保证返回值（如果有）顺序
 */
class ProxyGather {
  using CoVector = std::vector<coroutine::CoroID>;
  using RetMap = std::unordered_map<coroutine::CoroID, std::any>;
  using RetVector = std::vector<std::any>;
  CoVector co_vec_; ///< 协程ID数组, 记录启动的协程
  RetMap ret_map_;  ///< 返回字典, 记录调用方法返回值
  std::size_t wait_cnt_{0}; ///< 正在等待的调用数量, 记录等待未完成的调用数量
  coroutine::CoroID co_id_{
      coroutine::INVALID_CORO_ID}; ///< 当前协程ID, ProxyGather所在协程
  RetVector ret_vec_; ///< 调用返回数组, 按照启动顺序记录调用方法返回值
  rpc::Rpc *rpc_{nullptr}; ///< RPC实例

public:
  /**
   * 构造
   */
  ProxyGather(kratos::service::ServiceContext *ctx) {
    rpc_ = ctx->get_rpc();
    // 获取当前运行协程ID
    co_id_ = rpc_->coro_id();
  }
  /**
   * 执行调用协程
   *
   * \param fn 调用的协程函数, 返回值与proxy方法返回值相同
   * \exception std::runtime_error
   */
  template <typename T> void call(std::function<T()> fn) noexcept(false) {
    //
    // 建立一个协程, 后续将会被调度器调度
    //
    auto id = rpc_->spawn([this, fn](void *) {
      try {
        // 获取返回值
        ret_map_.emplace(rpc_->coro_id(), fn());
      } catch (CoroCancelException &) {
        // 协程被取消
        return;
      } catch (std::exception &ex) {
        // 发生异常, 推送异常并唤醒发起调用协程
        rpc_->coro_push(co_id_, ex, true);
        return;
      }
      // 唤醒发起调用协程
      rpc_->coro_push(co_id_, 0, true);
    });
    if (id == coroutine::INVALID_CORO_ID) {
      // 创建协程失败
      throw std::runtime_error("ProxyGather: Spawn coroutine failed");
    }
    wait_cnt_ += 1;
    // 记录调用协程ID
    co_vec_.push_back(id);
  }
  /**
   * 等待所有协程执行完成
   *
   * \param millions 等待超时时间, 毫秒
   * \exception std::runtime_error
   */
  void wait(std::time_t millions = 1000) noexcept(false) {
    if (!millions) {
      // 默认超时
      millions = 1000;
    }
    auto timeout = millions;       // 超时时间
    auto ts = get_millionsecond(); // 当前时间戳, 毫秒
    std::string except_reason;     // 异常原因
    CoVector co_vec(co_vec_);      // 协程ID数组, 临时对象
    while (wait_cnt_ > 0) {
      // 等待通知消息或超时被调度器唤醒
      auto mail = rpc_->coro_pop_wait(co_id_, timeout);
      if (!mail) {
        // 超时
        break;
      }
      wait_cnt_ -= 1;
      // 重新计算超时时间
      auto cur_ts = get_millionsecond();
      auto gap = cur_ts - ts;
      ts = cur_ts;
      if (gap < timeout) {
        // 调整超时时间
        timeout -= gap;
      } else {
        // 超时
        break;
      }
      if (mail.type() == typeid(std::exception)) {
        // 调用协程内发生异常
        except_reason +=
            "[" + std::string(std::any_cast<std::exception>(mail).what()) + "]";
      }
      // 获取调用协程ID
      auto sender_id = mail.getSender();
      for (auto it = co_vec.begin(); it != co_vec.end(); it++) {
        if (sender_id == *it) {
          // 删除已完成的协程的ID, 不论是正常结束还是发生异常
          co_vec.erase(it);
          break;
        }
      }
    }
    for (const auto &id : co_vec) {
      // 关闭超时或发生错误的协程
      rpc_->coro_cancel(id);
    }
    if (!except_reason.empty()) {
      // 调用的方法中有异常发生
      throw std::runtime_error("ProxyGather: " + except_reason);
    }
    if (!co_vec.empty()) {
      throw std::runtime_error("ProxyGather: call timeout");
    }
    //
    // 按照调用顺序生成返回值数组, 没有返回值的调用不会占用一个数组位置
    //
    for (const auto &id : co_vec_) {
      auto it = ret_map_.find(id);
      if (it != ret_map_.end()) {
        // 只记录有返回的调用返回值
        ret_vec_.emplace_back(it->second);
      }
    }
  }
  /**
   * 获取返回值
   *
   * \param index 调用顺序索引, index从零开始对应有返回值的调用顺序,
   * 没有返回值的调用将不会被计数
   */
  template <typename T> T get(std::size_t index) noexcept(false) {
    if (index >= ret_vec_.size()) {
      throw std::overflow_error("ProxyGather: Index out of range");
    }
    if (!ret_vec_[index].has_value()) {
      throw std::runtime_error("ProxyGather: Empty return value");
    }
    return std::any_cast<T>(ret_vec_[index]);
  }

private:
  /**
   * 获取当前毫秒返回值
   */
  auto get_millionsecond() -> std::time_t {
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                  std::chrono::system_clock::now().time_since_epoch())
                  .count();
    return ms;
  }
};

/**
 * 启动一个协程并执行fn
 *
 * \param fn 调用的协程函数, 返回值为void
 * \exception std::runtime_error
 */
template <> inline void ProxyGather::call(std::function<void()> fn) noexcept(false) {
  //
  // 建立一个协程, 后续将会被调度器调度
  //
  auto id = rpc_->spawn([this, fn](void *) {
    try {
      // 不关注返回值
      fn();
    } catch (CoroCancelException &) {
      // 协程被取消
      return;
    } catch (std::exception &ex) {
      // 发生异常, 推送异常并唤醒发起调用协程
      rpc_->coro_push(co_id_, ex, true);
      return;
    }
    rpc_->coro_push(co_id_, 0, true);
  });
  if (id == coroutine::INVALID_CORO_ID) {
    throw std::runtime_error("ProxyGather: Spawn coroutine failed");
  }
  wait_cnt_ += 1;
  // 记录调用协程ID
  co_vec_.push_back(id);
}

} // namespace util
} // namespace kratos
