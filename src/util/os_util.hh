﻿#pragma once

#include <ctime>
#include <memory>
#include <string>
#include <vector>

namespace kratos {
namespace util {

// transmit from domain to IP
// @param host domain host name
// @return IP
extern std::string get_host_ip(const std::string &host);
// returns the absobulte file path of executable binary
// @return the absobulte file path of executable binary
extern std::string get_binary_path();
// returns the executable binary file name
extern std::string get_binary_name();
// Collect all files in directory
// @param directory Directory
// @param suffix File extension
// @param fileNames result
// @retval true
// @retval false fail
extern bool get_file_in_directory(const std::string &directory,
                                  const std::string suffix,
                                  std::vector<std::string> &fileNames);
// Collect all file name in directory
// @param directory Directory
// @param suffix File extension
// @param fileNames result
// @retval true
// @retval false fail
extern bool get_file_name_in_directory(const std::string &directory,
                                       const std::string suffix,
                                       std::vector<std::string> &fileNames);
// Collect all files in directory recursively
// @param directory Directory
// @param suffix File extension
// @param fileNames result
// @retval true
// @retval false fail
extern bool
get_file_in_directory_recursive(const std::string &directory,
                                const std::string suffix,
                                std::vector<std::string> &fileNames);
// Check existence of directory
// @param path Directory
// @retval true
// @retval false
extern bool is_path_exists(const std::string &path);
// Remove file
// @param filePath The file path
// @retval true
// @retval false
extern bool remove_file(const std::string &filePath);
// Completes the path
// @param base the base directory
// @param sub sub-directory or file name
// @return the joined path
extern std::string complete_path(const std::string &base,
                                 const std::string &sub);
// Completes the URL path
// @param base the base directory
// @param sub sub-directory or file name
// @return the joined path
extern std::string complete_path_url(const std::string &base,
                                     const std::string &sub);
// Renames the file
// @param srcFileName old file name
// @param newFileName new file name
// @retval true
// @retval false
extern bool rename_file(const std::string &srcFileName,
                        const std::string &newFileName);
// Returns file's suffix
// @param fileName The file path
extern std::string get_file_ext(const std::string &fileName);
// Returns the random number between a and b
extern std::uint32_t get_random_uint32(std::uint32_t a, std::uint32_t b);
// 建立目录
extern auto make_dir(const std::string &path) -> bool;
/**
 * @brief 删除空目录
 */
extern auto rm_empty_dir(const std::string &path) -> bool;
/**
 * 获取进程PID.
 */
extern auto get_pid() -> int;
/**
 * 获取当前调用堆栈信息.
 */
extern auto get_current_stack_trace_info(int depth = 64) -> std::string;
/**
 * 获取文件最近一次的修改时间.
 */
extern auto get_last_modify_time(const std::string &path) -> std::time_t;
/**
 * @brief 获取文件指定行及上下related_line的文件内容
 * @param file_path 文件路径
 * @param line 行号
 * @param related_line 需要附带显示的上下行数
 * @param [OUT] content 文件内容
 * @return true或false
 */
extern auto get_file_content(const std::string &file_path, int line,
                             int related_line, std::string &content) -> bool;
/**
 * @brief 尝试锁定文件, 被锁定的文件在进程退出时才会被释放
 * @param file_path 文件路径
 * @return true或false
 */
extern auto try_lock_file(const std::string &file_path) -> bool;

/**
 * @brief 尝试解锁被try_lock_file锁定的文件
 * @param file_path 文件路径
 * @param is_remove 是否要删除
 * @return true或false
 */
extern auto try_unlock_file(const std::string &file_path, bool is_remove)
    -> bool;

/**
 * @brief 解锁所有被try_lock_file锁定的文件
 * @param is_remove 是否要删除
 * @return
 */
extern auto unlock_all_file(bool is_remove) -> void;
/**
 * @brief 获取MAC地址列表
 * @param [IN OUT] mac_addr_list MAC地址列表.
 */
extern auto get_mac_addr_list(std::vector<std::string> &mac_addr_list) -> void;
/**
 * @brief 获取IPV4地址列表
 * @param [IN OUT] mac_addr_list MAC地址列表.
 */
extern auto get_ip_addr_list(std::vector<std::string> &ip_addr_list) -> void;
/**
 * @brief 将MAC 10进制地址转换为64位无符号整数.
 */
extern auto get_mac_hash(const std::string &mac_addr) -> std::uint64_t;
/**
 * @brief 将IPV4地址转换为32位无符号整数.
 */
extern auto get_ip_hash(const std::string &ip_addr) -> std::uint32_t;
/**
 * @brief 设置当前.dll/.so搜索路径
 */
extern auto set_current_object_search_path(const std::string &path) -> void;
/**
 * @brief 获取network type
 */
extern auto get_network_type(const std::string &host) -> std::string;
/**
 * @brief 获取系统错误描述
 */
extern auto get_sys_error_str() -> std::string;
/**
 * @brief 获取模块文件(DLL/SO)是否是DEBUG版本
 */
extern auto is_debug_module(const std::string &mod_path) -> bool;
/**
 * @brief 获取最近一次错误描述
 */
extern auto get_last_errno_message() -> std::string;
/**
 * @brief 设置环境变量
 */
extern auto set_env(const char *name, const char *value) -> void;
/**
 * 检测端口是否被占用
 */
extern auto is_port_is_use(int port) -> bool;
/**
 * 获取端口范围内可用的端口
 */
extern auto get_any_port(int start, int end) -> int;
/**
 * 获取UUID
 */
extern auto create_uuid() -> std::string;
/**
 * 执行名命令并返回结果
 */
extern auto execute(const std::string &cmd_line) -> std::string;
/**
 * 获取环境变量
 */
extern auto get_env(const std::string &key) -> std::string;
/**
 * 获取Service UUID
 */
extern auto get_stub_uuid(const std::string &file_path) -> std::uint64_t;

} // namespace util
} // namespace kratos
