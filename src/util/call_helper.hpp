#pragma once

#include <algorithm>
#include <functional>
#include <memory>
#include <type_traits>
#include <vector>

namespace kratos {
namespace util {

/**
 * 调用组
 */
template <typename ClassT> class Scatter {
  using ClassPtr = std::shared_ptr<ClassT>;
  using InstanceVector = std::vector<ClassPtr>;
  InstanceVector instance_vec_; ///<  实例对象数组

public:
  /**
   * 构造
   */
  Scatter();
  /**
   * 析构
   */
  ~Scatter();
  /**
   * 加入
   *
   * \param o_ptr 对象实例
   * \return true or false
   */
  decltype(auto) join(ClassPtr o_ptr);
  /**
   * 从组内删除
   *
   * \param o_ptr 对象实例
   */
  decltype(auto) leave(ClassPtr o_ptr);
  /**
   * 遍历并依次调用组内对象的方法
   *
   * \param func 方法指针
   * \param args 参数列表
   * \return 方法返回值数组
   */
  template <typename Func, typename... Args>
  typename std::enable_if<
      !std::is_void<
          typename std::invoke_result<Func, ClassT *, Args...>::type>::value,
      std::vector<typename std::invoke_result<Func, ClassT *, Args...>::type>>::
      type
      dispatch(Func &&func, Args &&...args);
  /**
   * 遍历并依次调用组内对象的方法
   *
   * \param func 方法指针
   * \param args 参数列表
   */
  template <typename Func, typename... Args>
  typename std::enable_if<std::is_void<typename std::invoke_result<
                              Func, ClassT *, Args...>::type>::value,
                          void>::type
  dispatch(Func &&func, Args &&...args);
};

template <typename ClassT> Scatter<ClassT>::Scatter() {}

template <typename ClassT> Scatter<ClassT>::~Scatter() {}

template <typename ClassT> decltype(auto) Scatter<ClassT>::join(ClassPtr ptr) {
  return instance_vec_.emplace_back(ptr);
}

template <typename ClassT>
decltype(auto) Scatter<ClassT>::leave(ClassPtr o_ptr) {
  instance_vec_.erase(
      std::remove(instance_vec_.begin(), instance_vec_.end(), o_ptr),
      instance_vec_.end());
}

template <typename ClassT>
template <typename Func, typename... Args>
typename std::enable_if<
    !std::is_void<
        typename std::invoke_result<Func, ClassT *, Args...>::type>::value,
    std::vector<typename std::invoke_result<Func, ClassT *, Args...>::type>>::
    type
    Scatter<ClassT>::dispatch(Func &&func, Args &&...args) {
  using RetT = typename std::invoke_result<Func, ClassT *, Args...>::type;
  std::vector<RetT> vec;
  for (auto &o_ptr : instance_vec_) {
    if (o_ptr) {
      vec.emplace_back((o_ptr.get()->*func)(std::forward<Args>(args)...));
    }
  }
  return vec;
}

template <typename ClassT>
template <typename Func, typename... Args>
typename std::enable_if<std::is_void<typename std::invoke_result<
                            Func, ClassT *, Args...>::type>::value,
                        void>::type
Scatter<ClassT>::dispatch(Func &&func, Args &&...args) {
  for (auto &o_ptr : instance_vec_) {
    if (o_ptr) {
      (o_ptr.get()->*func)(std::forward<Args>(args)...);
    }
  }
}

} // namespace util
} // namespace kratos

template <typename T> using Scatter = kratos::util::Scatter<T>;
