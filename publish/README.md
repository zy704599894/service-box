# 发布

提供发布ServiceBox及仓库内服务的命令行工具。

# 配置service box

[service box配置说明](../README-configuration.md)

# 发布默认服务

在publish/bin目录下运行:

## Windows
```
python pub.py -c ../config/box.json
```

## Linux
```
python pub.py -c ../config/box-linux.json
```

