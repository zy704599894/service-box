# C# RPC

初始化C#仓库, 在src/repo/目录下执行:
```
python repo.py -t csharp -i
```
以example.idl为例，在src/repo/目录下执行:
```
python repo.py -t csharp -u example
python repo.py -t csharp -b example
```
将在```src/repo/usr/example```目录下生成C# RPC相关文件。

## 目录结构
+ src/repo/usr
    + csharp
        + rpc_framework RPC csharp框架
            + base 基础框架
            + buffer 读/写缓冲区
            + Lib 第三方库
            + pool 对象池
            + socket TCP网络客户端
        + rpc.register.cs RPC注册文件
        + example example.idl定义的相关服务
            + 目录 不同服务的实现文件目录
            + example.service.cs example.idl内服务的调用/返回封装
            + example.service.protobuf.serializer.cs google protobuf相关封装
            + ExampleService.cs google protobuf生成文件

# C# RPC框架

src/repo/usr/csharp/rpc_framework目录下的内容为C#版本的RPC实现。

# C# 版本的使用(Unity3D)
```
using UnityEngine;
using kratos;
using rpc;
using System;

public class Network : MonoBehaviour
{
    IRpc rpc_ = null; // RPC框架
    LoginProxy login_prx_ = null; // Login服务本地代理

    // 日志回调接口
    class Logger : kratos.ILogger
    {
        public void Debug(string log)
        {
            UnityEngine.Debug.Log(log);
        }

        public void Fail(string log)
        {
            UnityEngine.Debug.LogError(log);
        }

        public void Fatal(string log)
        {
            UnityEngine.Debug.LogError(log);
        }

        public void Info(string log)
        {
            UnityEngine.Debug.Log(log);
        }

        public void Warn(string log)
        {
            UnityEngine.Debug.LogWarning(log);
        }
    }

    // Start is called before the first frame update
    async void Start()
    {
        // 建立RPC框架
        rpc_ = RpcFactory.create();
        // 初始化，建立日志
        rpc_.Initialize(new Logger());
        // 设置自动重连
        rpc_.AutoReconnect = true;
        // 设置自动重连间隔
        rpc_.AutoReconnectIntvalMS = 1000;
        // 是否使用Tick来代替真实时间戳，可以减少获取系统时间戳的次数，但是会有一定的误差，但最大误差不超过一帧
        rpc_.UseTickAsTimestamp = true;
        // 启用协程调用，连接到服务端集群
        bool ret = await rpc_.Connect("127.0.0.1", 6888, 2000);
        // 调用完成后打印日志
        Debug.Log($"Connect result:{ret}");
        // 获取一个Login服务的本地代理
        login_prx_ = Proxy.GetLogin();
    }

    // Update is called once per frame
    void Update()
    {
        // 每帧运行RPC框架主循环
        rpc_.Update();
    }

    private async void LateUpdate()
    {

        if (login_prx_ != null)
        {
            // 启用协程，每帧调用一次Login::login方法
            var id = await login_prx_.login("123", "456");
            Debug.Log($"Login result:{id}");
        }
    }
}

```

